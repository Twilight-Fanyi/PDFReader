import os.path

from PyQt5 import QtWidgets

from ui.uicomponents.mainfunc.BookSelectionRelevantComponents.BookSelectionMainUI import BookSelectionMainUI
from util.database.ebook_database import EbookDatabase


class INIT:

    def __init__(self):
        # ⭐ 主目录
        self.home_path = os.path.join(os.path.expanduser("~"), "PDFReader")

        # plugins 插件目录
        self.plugin_path = os.path.join(self.home_path, "plugins")
        self._python_plugin_path = os.path.join(self.plugin_path, "py_plugin")
        self._javascript_plugin_path = os.path.join(self.plugin_path, "js_plugin")

        # config 配置目录
        self.config_path = os.path.join(self.home_path, "config")

        # database 数据库目录
        self.database_path = os.path.join(self.home_path, "database")

        # project 工程文件目录
        self.project_path = os.path.join(self.home_path, "project")

        # jscomponent 项目所用到的 JavaScript 组件
        self.jscomponent_path = os.path.join(self.home_path, "jscomponent")

        # 总配置文件 config.json
        self.main_config_path = os.path.join(self.home_path, "config.json")

        # 数据库文件 ebookinfo.db
        self.ebook_db_path = os.path.join(self.database_path, "ebookinfo.db")



        print(self.home_path)
        print(self.plugin_path)
        print(self._python_plugin_path)
        print(self._javascript_plugin_path)
        print(self.config_path)
        print(self.jscomponent_path)

    def init_dirs(self):
        if not os.path.exists(self.home_path):
            os.makedirs(self.home_path)
        if not os.path.exists(self.plugin_path):
            os.makedirs(self.plugin_path)
        if not os.path.exists(self._python_plugin_path):
            os.makedirs(self._python_plugin_path)
        if not os.path.exists(self._javascript_plugin_path):
            os.makedirs(self._javascript_plugin_path)
        if not os.path.exists(self.database_path):
            os.makedirs(self.database_path)
        if not os.path.exists(self.project_path):
            os.makedirs(self.project_path)
        if not os.path.exists(self.jscomponent_path):
            os.makedirs(self.jscomponent_path)

    def init_nods(self):
        if not os.path.exists(self.main_config_path):
            os.mknod(self.main_config_path)

    def init_database(self):
        if not os.path.exists(self.ebook_db_path):
            ebook_db = EbookDatabase()
            ebook_db.init_db()

    # TODO: 暂不启用，后续有时间再规划
    def init_jscomponent(self):
        if not os.path.exists(self.jscomponent_path):
            os.makedirs(self.jscomponent_path)



if __name__ == "__main__":
    init = INIT()
    init.init_dirs()
    init.init_nods()

    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = BookSelectionMainUI()
    UI.show()
    sys.exit(app.exec_())

