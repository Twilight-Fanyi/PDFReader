import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class SliderButton(QAbstractButton):
    def __init__(self,parent = None):
        super(SliderButton, self).__init__(parent)
        # 内部圆直径
        self.innerDiameter = 12
        # 是否选中标志位
        self.checked = False
        # 鼠标形状
        self.setCursor(Qt.PointingHandCursor)
        # 设置遮罩，固定形状
        bitmap = QBitmap('../btnMask.png')
        self.resize(bitmap.width(),bitmap.height())
        self.setMask(bitmap)
        # 内边距
        self.innerMargin = (self.height() - self.innerDiameter) / 2
        # x坐标偏移量
        self.offset = self.innerMargin
        # 内部圆背景色
        self.innerColor = QColor(89,89,89)
        # 内部圆背景色选中
        self.innerColorChecked = QColor(255,255,255)
        # 外部背景色
        self.outerColor = QColor(64,64,64)
        # 外部背景色选中
        self.outerColorChecked = QColor(51,153,255)
        # 定时器ID
        self.timeId = None

    def paintEvent(self,event):
        painter = QPainter(self)
        painter.setPen(Qt.NoPen)

        # 开启抗锯齿
        painter.setRenderHint(QPainter.Antialiasing)

        # 根据不同的选中状态切换内外颜色
        if self.checked:
            innerColor = self.innerColorChecked
            outerColor = self.outerColorChecked
        else:
            innerColor = self.innerColor
            outerColor = self.outerColor

        # 画外部圆角矩形
        painter.setBrush(outerColor)
        painter.drawRoundedRect(self.rect(),self.height() / 2,self.height() / 2)

        # 画内部圆形
        painter.setBrush(innerColor)
        painter.drawEllipse(QRectF(self.offset,self.innerMargin,self.innerDiameter,self.innerDiameter))

    def timerEvent(self, event):
        # 根据选中状态修改x坐标偏移值
        if self.checked:
            self.offset += 1
            if self.offset > (self.width() - self.innerDiameter - self.innerMargin):
                self.killTimer(self.timeId)
        else:
            self.offset -= 1
            if self.offset < self.innerMargin:
                self.killTimer(self.timeId)
        # 调用update，进行重绘
        self.update()

    def killTimer(self, timeId):
        # 删除定时器的同时，将timeId置为None
        super(SliderButton, self).killTimer(timeId)
        self.timeId = None

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.checked = not self.checked
            self.toggled.emit(self.checked)
            if self.timeId:
                self.killTimer(self.timeId)
            self.timeId = self.startTimer(5)

    def setChecked(self,checkState):
        # 如果状态没有变，不做处理
        if not checkState == self.checked:
            # 调用此方法改变状态不会触发动画，而是直接改变状态
            self.checked = checkState
            self.toggled.emit(checkState)
            if checkState:
                # 选中状态，偏移值设为最大值
                self.offset = self.width() - self.innerDiameter - self.innerMargin
            else:
                # 非选中状态，偏移值设置最小值
                self.offset = self.innerMargin
            # 更新界面
            self.update()

    def isChecked(self):
        return self.checked


class Window(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.resize(200, 100)
        self.switchButton = SliderButton()


    def onCheckedChanged(self, isChecked: bool):
        """ 开关按钮选中状态改变的槽函数 """
        text = '开' if isChecked else '关'
        self.switchButton.setText(text)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = Window()
    w.show()
    sys.exit(app.exec_())