import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QComboBox, QListView


class DropdownButton(QComboBox):
    def __init__(self):
        super().__init__()
        self.button_state = 0 # 0-关闭， 1-左功能栏， 2-右功能栏
        self.initUI()

    def initUI(self):
        # 创建垂直布局
        # layout = QVBoxLayout()

        # 创建下拉框
        # dropdown = QComboBox()
        self.addItem("关闭")
        self.addItem("左功能栏")
        self.addItem("右功能栏")
        self.setStyleSheet("""
            QComboBox {
                padding: 5px;
                border: 1px solid #CCCCCC;
                border-radius: 4px;
                background-color: white;
                width: 50px;
            }

            QComboBox::drop-down {
                subcontrol-origin: padding;
                subcontrol-position: right;
                width: 20px;
                border-top-right-radius: 4px;
                border-bottom-right-radius: 4px;
                background-color: white;
            }

            QComboBox::down-arrow {
                image: url(down-arrow.png);
                width: 12px;
                height: 12px;
            }

            QComboBox::down-arrow:on {
                image: url(up-arrow.png);
            }

            QComboBox QAbstractItemView {
                background-color: white;
                border: 1px solid #CCCCCC;
                outline: none;
                padding: 2px;
                selection-background-color: #0078D7;
                color: black;
            }

            QComboBox QAbstractItemView::item {
                height: 25px;  /* 调整选项的高度 */
                
            }

            QComboBox QAbstractItemView::item:selected {
                background-color: #0078D7;
                color: white;
            }
        """)
        self.setView(QListView())   # ⭐⭐
        # layout.addWidget(dropdown)

        # 连接信号和槽函数
        # self.currentIndexChanged.connect(self.handleSelectionChange)

        # self.setLayout(layout)
        # self.setWindowTitle("Dropdown Widget")
        self.show()

    def handleSelectionChange(self, index):
        selected_option = self.sender().currentText()
        if selected_option == "关闭":
            self.function1()
        elif selected_option == "左功能栏":
            self.function2()
        elif selected_option == "右功能栏":
            self.function3()

    def getButtonState(self):
        return self.button_state

    def function1(self):
        print("关闭")
        self.button_state = 0


    def function2(self):
        print("左功能栏")
        self.button_state = 1

    def function3(self):
        print("右功能栏")
        self.button_state = 2

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWindow = DropdownButton()
    sys.exit(app.exec_())
