import sys
from PyQt5.QtCore import Qt, QPropertyAnimation, QPoint, pyqtProperty, pyqtSignal, QEasingCurve
from PyQt5.QtGui import QPainter, QColor, QPen, QBrush
from PyQt5.QtWidgets import QWidget, QApplication


class ThreeStateSwitchButton(QWidget):
    switch_toggled = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setFixedSize(90, 30)
        self._switch_state = "OFF"
        self._switch_color = QColor(255, 0, 0)
        self._switch_pos = QPoint(0, 0)
        self._switch_animation = QPropertyAnimation(self, b"pos", self)
        self._switch_animation.setDuration(300)
        self._switch_animation.setEasingCurve(QEasingCurve.OutBack)
        self._switch_animation.finished.connect(self._on_animation_finished)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(QPen(Qt.NoPen))
        painter.setBrush(QBrush(QColor(200, 200, 200)))
        painter.drawRoundedRect(self.rect(), 15, 15)

        painter.setBrush(QBrush(self._switch_color))
        painter.drawEllipse(self._switch_pos.x(), self._switch_pos.y(), 30, 30)

    def mousePressEvent(self, event):
        if event.button() == Qt.RightButton:
            if event.pos().x() > self.width() / 2:
                self._move_right()
            else:
                self._move_left()

    def _move_right(self):
        if self._switch_state == "OFF":
            self._switch_animation.setStartValue(QPoint(0, 0))
            self._switch_animation.setEndValue(QPoint(60, 0))
            self._switch_animation.setDirection(QPropertyAnimation.Forward)
            self._switch_animation.start()

    def _move_left(self):
        if self._switch_state == "RIGHT":
            self._switch_animation.setStartValue(QPoint(60, 0))
            self._switch_animation.setEndValue(QPoint(0, 0))
            self._switch_animation.setDirection(QPropertyAnimation.Backward)
            self._switch_animation.start()

    def _on_animation_finished(self):
        if self._switch_state == "OFF":
            self._switch_state = "RIGHT"
            self._switch_color = QColor(0, 255, 0)  # Green color
        elif self._switch_state == "RIGHT":
            self._switch_state = "OFF"
            self._switch_color = QColor(255, 0, 0)  # Red color
        self.switch_toggled.emit(self._switch_state)

    @pyqtProperty(QPoint)
    def pos(self):
        return self._switch_pos

    @pos.setter
    def pos(self, pos):
        self._switch_pos = pos
        self.update()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    switch_button = ThreeStateSwitchButton()
    switch_button.show()
    sys.exit(app.exec_())
