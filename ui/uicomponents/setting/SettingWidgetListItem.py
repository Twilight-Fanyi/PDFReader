# -*- coding: utf-8 -*-
import sys

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QMouseEvent
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtWidgets import QFrame

from qfluentwidgets import SwitchButton

class SwitchButtonItem(SwitchButton):
    def mousePressEvent(self, event):
        super().mousePressEvent(event)
        # print("aaa")
        self.eventAfterMousePress()

    # def mousePressEvent(self, event, action):
    #     super().mousePressEvent(event)
    #     print("aaaction")

    def eventAfterMousePress(self):
        print("aaa")

class Ui_QSettingWidgetListItem(QWidget):

    def __init__(self):
        self.QLabelHeaderText = ''
        self.QLabelBodyText = ''
        self.SwitchButtonState = False
    def setupUi(self, QSettingWidgetListItem):
        QSettingWidgetListItem.setObjectName("QSettingWidgetListItem")
        QSettingWidgetListItem.resize(605, 74)
        QSettingWidgetListItem.setStyleSheet("*{\n"
"    background:rgb(255,255,255);\n"
"    font-size:15px;\n"
"    font-family:Century Gothic,sans-serif;\n"
"}\n"
"#QLabelHeader{\n"
"    font-size:20px;\n"
"    font-weight: bold\n"
"}\n"
"#QLabelBody{\n"
"color:rgb(132,133,133);\n"
"}\n"
"")

        self.gridLayout = QtWidgets.QGridLayout(QSettingWidgetListItem)
        self.gridLayout.setObjectName("gridLayout")
        self.QLabelHeader = QtWidgets.QLabel(QSettingWidgetListItem)
        self.QLabelHeader.setObjectName("QLabelHeader")
        self.gridLayout.addWidget(self.QLabelHeader, 0, 0, 1, 1)

        self.QLabelBody = QtWidgets.QLabel(QSettingWidgetListItem)
        self.QLabelBody.setObjectName("QLabelBody")
        self.gridLayout.addWidget(self.QLabelBody, 1, 0, 1, 1)

        self.SwitchButton = SwitchButtonItem(QSettingWidgetListItem)
        self.SwitchButton.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.SwitchButton.setObjectName("SwitchButton")
        self.gridLayout.addWidget(self.SwitchButton, 0, 1, 2, 1)

        self.getSwitchButtonState()

        self.retranslateUi(QSettingWidgetListItem)
        QtCore.QMetaObject.connectSlotsByName(QSettingWidgetListItem)

    def changeLabelHeader(self, text):
        self.QLabelHeaderText = text

    def changeLabelBody(self, text):
        self.QLabelBodyText = text

    def getSwitchButtonState(self):
        super(Ui_QSettingWidgetListItem, self)
        self.SwitchButtonState = self.SwitchButton.isChecked()





    def retranslateUi(self, QSettingWidgetListItem):
        _translate = QtCore.QCoreApplication.translate
        QSettingWidgetListItem.setWindowTitle(_translate("QSettingWidgetListItem", "SettingWidgetListItem"))
        self.QLabelHeader.setText(_translate("QSettingWidgetListItem", "TextLabel" if self.QLabelHeaderText=='' else self.QLabelHeaderText))
        self.QLabelBody.setText(_translate("QSettingWidgetListItem", "TextLabel" if self.QLabelBodyText=='' else self.QLabelBodyText))
        self.SwitchButton.setOnText(_translate("QSettingWidgetListItem", "开"))
        self.SwitchButton.setOffText(_translate("QSettingWidgetListItem", "关"))



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_QSettingWidgetListItem()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
