import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QRadioButton, QButtonGroup

class RadioButtonGroup(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        # 创建垂直布局
        layout = QVBoxLayout()

        # 创建按钮组
        button_group = QButtonGroup()

        # 创建按钮1
        button1 = QRadioButton("State 1")
        button1.setStyleSheet("QRadioButton::indicator { subcontrol-origin: padding; subcontrol-position: bottom center; }")
        button_group.addButton(button1)
        layout.addWidget(button1)

        # 创建按钮2
        button2 = QRadioButton("State 2")
        button2.setStyleSheet("QRadioButton::indicator { subcontrol-origin: padding; subcontrol-position: bottom center; }")
        button_group.addButton(button2)
        layout.addWidget(button2)

        # 创建按钮3
        button3 = QRadioButton("State 3")
        button3.setStyleSheet("QRadioButton::indicator { subcontrol-origin: padding; subcontrol-position: bottom center; }")
        button_group.addButton(button3)
        layout.addWidget(button3)

        self.setLayout(layout)
        self.setWindowTitle("Three-State Switch Buttons")
        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWindow = RadioButtonGroup()
    sys.exit(app.exec_())
