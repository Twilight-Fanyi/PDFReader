import sys

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import Qt, QPropertyAnimation, QRect, pyqtProperty, pyqtSignal, QStringListModel
from PyQt5.QtGui import QPainter, QColor, QPen, QBrush, QPixmap
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QFileDialog, QAction


from util.parsers.pdf_parser import PDF_Parser

"""
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
This class is FeatureBarWidget, 
which is a bar of BookSelectionRelevantComponent
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
"""





class FeatureBarWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setObjectName("FeatureBarWidget")
        self.setFixedSize(200, 650)
        self.setContentsMargins(0, 0, 0, 0)

        # self._item_1_name = "打开"
        # self._item_2_name = "导入"

        self.setupUi()
        self.setStyleSheet("*{font-size:20px;"
                           "font-family:SimHei,sans-serif;"
                           "border-style:dotted dashed solid double;"
                           "background:rgb(247,248,250)"
                           "}"
                           "QPushButton{"
                           "background:rgb(255,255,255);"
                           "border: 1px solid #dcdfe6;"
                           "}"
                           "QPushButton:hover {"
                            "background-color: #ecf5ff;"
                            "color: #409eff;}"
                           )


        # self.setFirstPageAsCover()

    def setupUi(self):
        self.main_widget_layout = QtWidgets.QVBoxLayout(self)
        self.main_widget_layout.setDirection(QtWidgets.QVBoxLayout.Direction.TopToBottom)
        self.main_widget_layout.setContentsMargins(0, 0, 0, 0)

        # self.main_widget_layout = QtWidgets.QVBoxLayout(self)
        # self.list_widget = QtWidgets.QListWidget()
        # self.main_widget_layout.addWidget(self.list_widget)
        # self.list_item_1 = FeatureBarListItem()
        # self.main_widget_layout.addWidget(self.list_item_1)
        # self.list_item_2 = FeatureBarListItem()
        # self.main_widget_layout.addWidget(self.list_item_2)
        # self.list_item_1 = QtWidgets.QListWidgetItem()
        # self.list_item_1.setText(self._item_1_name)
        # self.list_widget.addItem(self.list_item_1)
        #
        #
        # self.list_item_2 = QtWidgets.QListWidgetItem()
        # self.list_item_2.setText(self._item_2_name)
        # self.list_widget.addItem(self.list_item_2)

        # self.button  = QtWidgets.QPushButton()
        # self.button.setStyleSheet("*{background:rgb(0,255,255)}")
        # self.button.setText("Click")
        #
        # self.button1 = QtWidgets.QPushButton()
        # self.button1.setStyleSheet("*{background:rgb(0,255,255)}")
        # self.button1.setText("Click")

        # self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        # self.main_widget_layout.addWidget(self.button)
        # self.main_widget_layout.addWidget(self.button1)


        # self.main_widget_layout.addItem(self.spacer)

    def addSpacer(self):
        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.main_widget_layout.addItem(self.spacer)

    # 测试用，新 UI 建议新开
    def openFileAction(self):
        book_path, info = QtWidgets.QFileDialog.getOpenFileName()

    def addFeatureItem(self, feature_name, feature):
        button = QtWidgets.QPushButton()
        button.setFixedHeight(40)
        button.setContentsMargins(0, 0, 0, 0)
        button.setText(feature_name)
        button.clicked.connect(feature)
        self.main_widget_layout.addWidget(button)


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = FeatureBarWidget()
    UI.addFeatureItem("Feature", UI.openFileAction)
    UI.addSpacer()
    UI.show()
    sys.exit(app.exec_())
