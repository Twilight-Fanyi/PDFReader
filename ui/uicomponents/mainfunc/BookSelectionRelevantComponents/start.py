import sys

from PyQt5 import QtWidgets

from ui.uicomponents.mainfunc.BookSelectionRelevantComponents.BookSelectionMainUI import BookSelectionMainUI

app = QtWidgets.QApplication(sys.argv)
UI = BookSelectionMainUI()
UI.show()
sys.exit(app.exec_())