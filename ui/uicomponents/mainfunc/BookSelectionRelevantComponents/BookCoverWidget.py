import sys

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import Qt, QPropertyAnimation, QRect, pyqtProperty, pyqtSignal, QStringListModel
from PyQt5.QtGui import QPainter, QColor, QPen, QBrush, QPixmap
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QFileDialog, QAction

from ui.uicomponents.mainfunc.BookSelectionRelevantComponents.BookCoverSingleWidget import BookCoverSingleWidget
from util.database.ebook_database import EbookDatabase


from util.parsers.pdf_parser import PDF_Parser

"""
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
This class is FeatureBarWidget, 
which is a bar of BookSelectionRelevantComponent
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
"""



class BookCoverWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setObjectName("BookCoverWidget")
        self.setFixedSize(900, 650)
        self.setContentsMargins(0, 0, 0, 0)
        self._book_number = 6
        self._number_of_each_row = 4

        self._item_1_name = "打开"
        self._item_2_name = "导入"

        self.setupUi()
        # self.setStyleSheet("*{font-size:20px;"
        #                    "font-family:SimHei,sans-serif;"
        #                    "border-style:dotted dashed solid double"
        #                    "}"
        #                    "QPushButton{"
        #                    "background:rgb(0,255,255)"
        #                    "}")

        # self.setStyleSheet("QScrollArea{background-color:transparent;}")
        # self.setStyleSheet("#ScrollRollAreaWidget{background-color:blud;}")
        self.setStyleSheet('''
            QScrollBar:vertical {
                background-color: #f0f0f0;
                width: 3px;
                margin: 0px;
            }

            QScrollBar::handle:vertical {
                background-color: #c0c0c0;
                min-height: 20px;
            }

            QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {
                background-color: #f0f0f0;
            }

            QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {
                background-color: #f0f0f0;
                border: none;
                width: 15px;
                height: 15px;
                subcontrol-position: top;
                subcontrol-origin: margin;
            }
            QScrollArea{
                background-color:transparent;
            }
            
        ''')




        # self.setFirstPageAsCover()

    # TODO: content 布局更改为网格状
    def setupUi(self):
        self.main_widget_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.main_widget_layout)
        # self.main_widget_layout.setDirection(QtWidgets.QVBoxLayout.Direction.TopToBottom)
        # self.main_widget_layout.setContentsMargins(0, 0, 0, 0)
        self.scroll_roll_area_widget = QtWidgets.QScrollArea()

        self.scroll_roll_area_widget.setObjectName("ScrollRollAreaWidget")


        self.scroll_roll_area_widget.setStyleSheet("background-color:white;")

        # 设置 QScrollArea 的子部件在绘制顺序上在窗口之上
        self.scroll_roll_area_widget.setWidgetResizable(True)

        # 设置窗口小部件的背景颜色
        self.scroll_roll_area_widget.viewport().setStyleSheet("background-color: transparent;")

        self.scroll_roll_area_widget.verticalScrollBar().setStyleSheet("")
        self.content_widget = QtWidgets.QWidget()
        self.content_widget.setObjectName("ContentWidget")
        # self.content_widget.setStyleSheet("background-color:white")
        self.main_widget_layout.addWidget(self.scroll_roll_area_widget)
        self.content_widget = QtWidgets.QWidget()
        self.scroll_layout = QtWidgets.QGridLayout(self.content_widget)
        self.scroll_layout.setAlignment(Qt.AlignTop)
        self.scroll_layout.setColumnStretch(3, 1)

        self.scroll_roll_area_widget.setWidget(self.content_widget)
        self.scroll_roll_area_widget.setWidgetResizable(True)   # ⭐设置窗口小部件可调整大小
        self.main_widget_layout.addWidget(self.scroll_roll_area_widget)


    ## 测试用：向 QScrollArea 添加 Item Pass
    def addItemTest(self):
        for i in range(100):
            label = QtWidgets.QLabel(f"Label {i}")
            self.scroll_layout.addWidget(label)

    def getWidgetWidth(self):
        print(self.width())
        return self.width()

    def addBookCoverWidget(self):
        for i in range(self._book_number):
            book_cover_widget = BookCoverSingleWidget('F:/4.pdf')
            self.scroll_layout.addWidget(book_cover_widget,  i // self._number_of_each_row, i % self._number_of_each_row)

    # TODO: get the number of book from DB
    def addBookCoverWidgetFromDB(self):
        ebook_database = EbookDatabase()
        book_info = ebook_database.select_book_info()
        self._book_number = len(book_info)
        for i in range(self._book_number):
            book_name = book_info[i][0]
            book_path = book_info[i][2]
            book_cover_widget = BookCoverSingleWidget(book_path)
            # print(book_path)
            # print(book_name)
            book_cover_widget.setBookName(book_name)
            book_cover_widget.setFirstPageAsCoverFromPath(book_path)
            self.scroll_layout.addWidget(book_cover_widget,  i // self._number_of_each_row, i % self._number_of_each_row)



if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = BookCoverWidget()
    # UI.addFeatureItem("Feature", UI.openFileAction)
    UI.addBookCoverWidgetFromDB()
    UI.show()
    sys.exit(app.exec_())
