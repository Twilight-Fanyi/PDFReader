import sys

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QPropertyAnimation, QRect, pyqtProperty, pyqtSignal
from PyQt5.QtGui import QPainter, QColor, QPen, QBrush, QPixmap, QPalette
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QFileDialog

from ui.uicomponents.mainfunc.PDFReaderMainWindowComponent.Ui_PDFReaderMainWindow_light import Ui_PDFReaderMainWindow
from util.parsers.pdf_parser import PDF_Parser

"""
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
This class is BookCoverSingleWidget, 
which contains BookCoverSingleWidget UI and relevant functions of BookCoverSingleWidget
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
"""


class BookCoverSingleWidget(QWidget):
    def __init__(self, path, parent=None):
        super().__init__(parent)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setObjectName("BookCoverSingleWidget")
        self.setFixedSize(200, 300)
        self.book_path = path
        self.windows = []
        self.setContentsMargins(10,10,10,10)
        self._book_name = ''
        self._book_cover_image = QPixmap("BookCover.png")

        # self._book_path, info = QFileDialog.getOpenFileName()
        # print(self._book_path)
        # self._book_cover_image = PDF_Parser.generate_first_page()
        # self.setFirstPageAsCover()

        self.setupUi()
        # self.setContentsMargins(10, 10, 10, 10)


        # self.setFirstPageAsCover()

    def setupUi(self):
        self.main_widget_layout = QtWidgets.QGridLayout(self)  # ⭐

        self.book_cover_image_label = QtWidgets.QLabel()
        self.book_cover_image_label.setObjectName("BookCoverImageLabel")
        self.main_widget_layout.addWidget(self.book_cover_image_label)
        self.book_cover_image_label.setPixmap(self._book_cover_image)
        self.book_cover_image_label.setAlignment(Qt.AlignCenter)

        # self.book_name_label = QtWidgets.QLabel()
        # self.book_name_label.setObjectName("BookNameLabel")
        # self.book_name_label.setFixedSize(300, 50)
        # self.main_widget_layout.addWidget(self.book_name_label)

        self.book_name_line_edit = QtWidgets.QLineEdit()
        self.book_name_line_edit.setObjectName("BookNameLineEdit")
        self.main_widget_layout.addWidget(self.book_name_line_edit)

        # self.retranslateUi()

    # def retranslateUi(self):
    # self.book_cover_image_label.setText('1111')
    # self.book_name_label.setText('22222')

    # TODO: read QPixmap from DB
    def setFirstPageAsCover(self):
        parser = PDF_Parser(self._book_path)
        parser.read_book()
        # print(type(parser.generate_first_page()))
        self._book_cover_image = QPixmap.fromImage(parser.generate_first_page())
        self._book_cover_image = self._book_cover_image.scaled(200, 300)

        # self._book_cover_image = PDF_Parser.generate_first_page(self)
        # self.book_cover_image_label.setPixmap(self._book_cover_image)

    # def setBookCoverImage(self):
    #     self.book_cover_image_label = self.

    def setFirstPageAsCoverFromPath(self, path):
        parser = PDF_Parser(self.book_path)
        parser.read_book()
        # print(type(parser.generate_first_page()))
        self._book_cover_image = QPixmap.fromImage(parser.generate_first_page())
        self._book_cover_image = self._book_cover_image.scaled(200, 300)
        self.book_cover_image_label.setPixmap(self._book_cover_image)

    def setBookName(self, bookname):
        self._book_name = bookname
        self.book_name_line_edit.setText(bookname)

    def mousePressEvent(self, event):
        self.startNewPDFReaderMainWindow(self.book_path, self._book_name)

    def startNewPDFReaderMainWindow(self, path, bookname):

        print(path)
        print(bookname)
        MainWindow = QtWidgets.QMainWindow()
        self.windows.append(MainWindow)
        Ui = Ui_PDFReaderMainWindow()

        Ui.setBookPath(path)
        Ui.setBookName(bookname)
        # self._feature_list = Ui.getActions()
        # MainWindow = QtWidgets.QWidget()
        # Ui = ContentTabWidget()
        # self.receive_function(Ui.toggleActionTriggered)
        # Ui.changeActionLoadFunc(self.function_to_call)
        Ui.setupUi(MainWindow)
        # Ui.showMaximized()
        # Ui.loadPythonFeatures(MainWindow, self.function_to_call)
        MainWindow.show()

    # def mousePressEvent(self, event, function):
    #     function

    def enterEvent(self, event):
        # self.setStyleSheet("#BookCoverSingleWidget{background-color: lightblue;}")
        self.setStyleSheet("*{background-color: lightblue;}")

    def leaveEvent(self, event):
        self.setStyleSheet("#BookCoverSingleWidget{background-color: white;}")


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = BookCoverSingleWidget('F:/sample.pdf')
    UI.setFirstPageAsCoverFromPath('F:/sample.pdf')
    UI.show()
    sys.exit(app.exec_())
