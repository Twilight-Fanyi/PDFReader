import importlib.util
import os.path
import sys

from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import Qt, QPropertyAnimation, QRect, pyqtProperty, pyqtSignal, QStringListModel
from PyQt5.QtGui import QPainter, QColor, QPen, QBrush, QPixmap
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QFileDialog, QAction


from ui.uicomponents.mainfunc.BookSelectionRelevantComponents.BookCoverWidget import BookCoverWidget
from ui.uicomponents.mainfunc.BookSelectionRelevantComponents.FeatureBarWidget import FeatureBarWidget
# from ui.uicomponents.mainfunc.BookSelectionRelevantComponents.PluginStore import PluginStore
from util.database.ebook_database import EbookDatabase

from util.parsers.pdf_parser import PDF_Parser

from datetime import datetime
from ui.uicomponents.mainfunc.PDFReaderMainWindowComponent.Ui_PDFReaderMainWindow_light import Ui_PDFReaderMainWindow
from util.plugin.PluginStoreComponents.PluginStore import PluginStore

"""
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
This class is BookSelectionMainUI, 
which is the main UI of BookSelection Component
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
"""

class BookSelectionMainUI(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setObjectName("BookSelectionMainUI")
        # self.setFixedSize(1100, 650)
        self.windows = []
        self._feature_list = []
        self.setupUi()
        self.setStyleSheet("BookSelectionMainUI{background:rgb(255,255,255)}")

    def setupUi(self):
        self.main_widget_layout = QtWidgets.QHBoxLayout(self)
        # self.main_widget_layout.setContentsMargins(0, 0, 0, 0)
        # self.book_cover_widget = BookCoverWidget()
        self.feature_bar_widget = FeatureBarWidget()

        self.feature_bar_widget.addFeatureItem("打开文件", self.openFileFeature_Button)
        self.feature_bar_widget.addFeatureItem("导入文件", self.importFileFeature_Button)
        self.feature_bar_widget.addFeatureItem("管理插件", self.openPluginController_Button)
        self.book_cover_widget = BookCoverWidget()
        self.main_widget_layout.addWidget(self.feature_bar_widget)
        self.main_widget_layout.addWidget(self.book_cover_widget)
        self.book_cover_widget.addBookCoverWidgetFromDB()
        self.feature_bar_widget.addSpacer()
        # self.main_widget_layout.addWidget(self.book_cover_widget)

    def openPluginController_Button(self):
        self.plugin_store = PluginStore()
        # self.windows.append(plugin_store)
        self.plugin_store.show()

    def openFileFeature_Button(self):
        book_path, info = QtWidgets.QFileDialog.getOpenFileName(self, "打开", "C:/", "PDF File (*.pdf)")
        book_name = str(os.path.basename(book_path)).split(".")[0]
        timestamp = self.currentMillisecondTimestamp()

        # print(book_name)
        # print(book_path)
        # print(timestamp)
        if book_name != '' and book_path != '':
            self.addBookToDatabase(book_name, timestamp, book_path)
            self.book_cover_widget.addBookCoverWidgetFromDB()
            self.startNewPDFReaderMainWindow(book_path, book_name)

        # return book_name, book_path


    def importFileFeature_Button(self):
        books_path, infos = QtWidgets.QFileDialog.getOpenFileNames(self, "打开", "C:/", "PDF Files (*.pdf)")
        timestamp = self.currentMillisecondTimestamp()
        for book_path in books_path:
            book_name = str(os.path.basename(book_path)).split(".")[0]
            self.addBookToDatabase(book_name, timestamp, book_path)
        self.book_cover_widget.addBookCoverWidgetFromDB()


    def currentMillisecondTimestamp(self):
        current_time = datetime.now()
        current_timestamp = current_time.timestamp()
        return current_timestamp

    def parseBookCoverFromPath(self, path):
        pdf_parser = PDF_Parser(path)
        pdf_parser.read_book()
        pixmap = QPixmap.fromImage(pdf_parser.generate_first_page())
        return pixmap

    def addBookToDatabase(self, bookname, timestamp, bookpath):
        ebook_database = EbookDatabase()
        try:
            ebook_database.insert_book_info(bookname, timestamp, bookpath)
            return True
        except:
            print("添加新书到数据库失败")
            return False

    def startNewPDFReaderMainWindow(self, path, bookname):

        print(path)
        print(bookname)
        MainWindow = QtWidgets.QMainWindow()
        self.windows.append(MainWindow)
        Ui = Ui_PDFReaderMainWindow()

        Ui.setBookPath(path)
        Ui.setBookName(bookname)
        # self._feature_list = Ui.getActions()
        # MainWindow = QtWidgets.QWidget()
        # Ui = ContentTabWidget()
        self.receive_function(Ui.toggleActionTriggered)
        Ui.changeActionLoadFunc(self.function_to_call)
        Ui.setupUi(MainWindow)
        # Ui.loadPythonFeatures(MainWindow, self.function_to_call)
        MainWindow.show()

    # ⭐⭐⭐有点巧妙的用法，少写了几十行代码
    def receive_function(self, func):
        self.function_to_call = func

    def openFile(self):
        file, ok = QtWidgets.QFileDialog.getOpenFileName()

    def toggleActionTriggered(self, checked):
        action = self.sender()
        widget = self.getWidgetOfAction(action)
        # widget_clone = widget.clone()
        print(widget)
        if checked:
            # if self.findChild('LeftBar') is not None:
            self._left_bar.hide()
            self.setOtherActionOff(action)
            self.showFeatureToolBarLeftWithWidget(widget)
            # self._left_bar.show()
            # self.showFeatureToolBarLeft()
            print("选中")
        else:
            self.cancelFeatureToolBarLeft()
            print("未选中")


    def getWidgetOfAction(self, action):
        for item in self._actionInfoList:
            if item[0].objectName == action.objectName:
                return item[1]

    def setOtherActionOff(self, action):
        for item in self._actionInfoList:
            if item[0].objectName != action.objectName:
                item[0].setChecked(False)

if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = BookSelectionMainUI()
    UI.show()

    sys.exit(app.exec_())
