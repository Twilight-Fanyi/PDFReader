
import sys
import warnings

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import Qt, QPropertyAnimation, QRect, pyqtProperty, pyqtSignal, QStringListModel
from PyQt5.QtGui import QPainter, QColor, QPen, QBrush, QPixmap
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QFileDialog, QAction


from util.parsers.pdf_parser import PDF_Parser

"""
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
This class is FeatureBarWidget, 
which is a bar of BookSelectionRelevantComponent
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
"""





class FeatureBarWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setObjectName("FeatureBarWidget")
        # self.setFixedSize(200, 650)
        self.setFixedWidth(150)
        self.setContentsMargins(0, 0, 0, 0)

        # self._item_1_name = "打开"
        # self._item_2_name = "导入"

        self.setupUi()
        self.setStyleSheet("*{font-size:20px;"
                           "font-family:SimHei,sans-serif;"
                           "border-style:dotted dashed solid double"
                           "}"
                           "QPushButton{"
                           "background:rgb(0,255,255)"
                           "}")


        # self.setFirstPageAsCover()

    def setupUi(self):
        self.main_widget_layout = QtWidgets.QVBoxLayout(self)
        self.main_widget_layout.setDirection(QtWidgets.QVBoxLayout.Direction.TopToBottom)
        self.main_widget_layout.setContentsMargins(0, 0, 0, 0)


    def addSpacer(self):
        self.spacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.main_widget_layout.addItem(self.spacer)

    # 测试用，新 UI 建议新开
    def openFileAction(self):
        book_path, info = QtWidgets.QFileDialog.getOpenFileName()


    def addFeatureItem(self, feature_name, feature):
        warnings.warn("⭐废弃的方法，为了兼容其他组件使用，新方法使用addFeatureWithOnlyButton方法代替⭐")
        button = QtWidgets.QPushButton()
        button.setText(feature_name)
        button.clicked.connect(feature)
        self.main_widget_layout.addWidget(button)

    def addFeatureWithOnlyButton(self, feature_name, feature):
        button = QtWidgets.QPushButton()
        button.setObjectName("FeatureWithOnlyButton_Button")
        button.setText(feature_name)
        button.clicked.connect(feature)
        self.main_widget_layout.addWidget(button)

    def addFeatureWithLabelAndButton(self, label_text, button_text, feature):
        widget = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout()
        widget.setLayout(layout)
        label = QtWidgets.QLabel()
        button = QtWidgets.QPushButton()
        label.setText(label_text)
        button.setText(button_text)
        label.setObjectName("FeatureWithLabelAndButton_Label")
        button.setObjectName("FeatureWithLabelAndButton_Button")
        button.clicked.connect(feature)
        layout.addWidget(label)
        layout.addWidget(button)
        self.main_widget_layout.addWidget(widget)


    def setFeatureBarWidgetWidth(self, width):
        self.setFixedWidth(width)


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = FeatureBarWidget()
    UI.addFeatureItem("Feature", UI.openFileAction)
    UI.addFeatureWithLabelAndButton("a", "b", UI.openFileAction)
    UI.addSpacer()
    UI.show()
    sys.exit(app.exec_())
