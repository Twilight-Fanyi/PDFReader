# -*- coding: utf-8 -*-
import importlib
import importlib.util
import json
import os
from time import sleep


from PyQt5 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets
from PyQt5.QtWidgets import QFileDialog, QDesktopWidget
from PyQt5.QtGui import QImage, QPixmap, QTransform
import fitz

from ui.uicomponents.mainfunc.PDFReaderMainWindowComponent.FeatureBarWidget import FeatureBarWidget
# from Ui_ContentTabWidget import ContentTabWidget
# from ui.mdict.import_mdict_file import MainWindow
# from ui.uicomponents.mainfunc.AboutSoftware.Ui_AboutSoftware import Ui_AboutSoftware as uia
# from util.pdfreader.PDFReaderWidget_bak import PDFReaderWidget
# from util.pdfreader.PDFReaderWidget import PDFReaderWidget
from util.pdfreader.PDFReaderWidgetWithNative import PDFReaderWidgetWithNative
from util.note.mdnote.Ui_Mdnote import Ui_Mdnote
import sys

class Ui_PDFReaderMainWindow(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        super(Ui_PDFReaderMainWindow, self).__init__()
        self._book_path = ''
        self._book_name = ''
        self._desktop_width, self._desktop_height = self.getDesktopSize()
        self._feature_list = []
        self._left_bar_feature_list = []
        self._right_bar_feature_list = []
        self._left_bar = QtWidgets.QWidget()
        self._right_bar = QtWidgets.QWidget()

        self._mdeditor_path = os.path.join(os.path.expanduser("~"), "PDFReader\\jscomponent\\mdeditor\\index.html")
        # print(self._mdeditor_path)
        self.plugin_path = os.path.join(os.path.expanduser("~"), "PDFReader", "plugins")
        self._python_plugin_path = os.path.join(self.plugin_path, "py_plugin")
        self._javascript_plugin_path = os.path.join(self.plugin_path, "js_plugin")
        # print(self.plugin_path)
        # print(self._python_plugin_path)
        # print(self._javascript_plugin_path)

        sys.path.append(self._python_plugin_path)
        sys.path.append(self._javascript_plugin_path)

        if not os.path.exists(self.plugin_path):
            os.makedirs(self.plugin_path)
        if not os.path.exists(self._python_plugin_path):
            os.makedirs(self._python_plugin_path)
        if not os.path.exists(self._javascript_plugin_path):
            os.makedirs(self._javascript_plugin_path)

        self._python_plugin_list = []
        self._javascript_plugin_list = []

        self._python_plugin_info_list = []
        self._javascript_plugin_info_list = []

        self._actionInfoList = []

        self.getPythonPluginList()
        self.getJavaScriptPluginList()
        self.getPythonPluginInfoList()
        self.getJavaScriptPluginInfoList()
        self.actionFunc = self.toggleActionTriggered



    def setupUi(self, PDFReaderMainWindow):
        PDFReaderMainWindow.setObjectName("PDFReaderMainWindow")
        PDFReaderMainWindow.setWindowModality(QtCore.Qt.NonModal)
        PDFReaderMainWindow.resize(815, 732)
        # PDFReaderMainWindow.resize(self._desktop_width, self._desktop_height)
        PDFReaderMainWindow.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        PDFReaderMainWindow.setStyleSheet(
"#PDFReaderMainWindow{\n"
"background:rgba(255,255,255,1)\n"
"}\n"
"QStatusBar{\n"
"background:rgba(248,249,251,1);\n"
"border-color:rgba(183,185,185,1);\n"
"}\n"
"QMenuBar{\n"
"background:rgba(248,249,251,1);\n"
"border-color:rgba(183,185,185,1);\n"
"}\n"
"QToolBar{\n"
"background:rgba(248,249,251,1);\n"
"border-color:rgba(183,185,185,1);\n"
"}\n"
"QToolButton{\n"
"background:rgba(248,249,251,1);\n"
"}\n"
"#toolBarRight{\n"
"margin-top:4px;\n"
"margin-bottom:2px;\n"
"}\n"
"\n"
"#toolBarLeft{\n"
"margin-top:4px;\n"
"margin-bottom:2px;\n"
"}\n"
"\n"
"#menuBarLeft{\n"
"\n"
"}\n"
"\n"
"#menuBarRight{\n"
"\n"
"}\n"
"\n"
"Frame{\n"
"margin-top:2px;\n"
"margin-bottom:2px;\n"
"margin-left:2px;\n"
"margin-right:2px;\n"
"}"
                                          )
        self.centralwidget = QtWidgets.QWidget(PDFReaderMainWindow)
        self.centralwidget.setObjectName("centralwidget")

        # self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        # self.gridLayout.setObjectName("gridLayout")
        # self._pdf_reader_widget = PDFReaderWidget()
        # self.gridLayout.addWidget(self._pdf_reader_widget)

        # self.features_toolbar_left_widget = QtWidgets.QWidget()
        # self.features_toolbar_right_widget = QtWidgets.QWidget()

        # self.features_toolbar_left_layout = QtWidgets.QHBoxLayout(self.features_toolbar_left_widget)
        # self.features_toolbar_right_layout = QtWidgets.QGridLayout(self.features_toolbar_right_widget)

        settings = QtWebEngineWidgets.QWebEngineSettings.globalSettings()

        settings.setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.LocalContentCanAccessFileUrls, True
        )

        settings.setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.JavascriptEnabled, True
        )


        # ⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
        self.main_widget_layout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.main_widget_layout.setObjectName("main_widget_layout")
        self.pdf_reader_widget = PDFReaderWidgetWithNative(self._book_path)
        self.pdf_reader_widget.setObjectName("PDFReaderWidget")
        self.mdnote_widget = Ui_Mdnote(self._book_path, self._book_name)
        self.mdnote_widget.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.LocalContentCanAccessFileUrls, True
        )
        self.mdnote_widget.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.LocalStorageEnabled, True
        )

        self.mdnote_widget.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.LocalContentCanAccessRemoteUrls, True
        )

        self.mdnote_widget.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.PluginsEnabled, True)
        self.mdnote_widget.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.PdfViewerEnabled, True)

        self.mdnote_widget.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.JavascriptEnabled, True
        )
        # self.mdnote_widget.resetUrl("E:/Projects/QT/PDFReader/util/note/mdnote/mdeditor/index.html")
        # self.mdnote_widget.resetUrl("C:\\Users\\Administrator\\PDFReader\\jscomponent\\mdeditor\\index.html")
        self.mdnote_widget.resetUrl(self._mdeditor_path)
        # self.main_widget_layout.insertWidget(0, self.features_toolbar_left_widget)
        # self.main_widget_layout.insertWidget(1, self.features_toolbar_right_widget)


        # self.main_widget_layout.addWidget(self.pdf_reader_widget)
        self.main_widget_layout.insertWidget(1, self.pdf_reader_widget)
        # self.main_widget_layout.addWidget(self.mdnote_widget)
        self.main_widget_layout.insertWidget(2, self.mdnote_widget)
        # self.main_widget_layout.insertWidget(3, self.features_toolbar_right_widget)



        # ⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐

        # 添加 frame
        # self.frame = QtWidgets.QFrame(self.centralwidget)
        # self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        # self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        # self.frame.setObjectName("frame")
        # self.gridLayout.addWidget(self.frame, 0, 1, 1, 1)

        # self.tabwidget = QtWidgets.QTabWidget(self.centralwidget)
        # self.tabwidget.setObjectName("tabwidget")
        # self.gridLayout.addWidget(self.tabwidget, 0, 1, 1, 1)

        # # self.tab = ContentTabWidget()
        #
        # self.tab.setObjectName("tab")
        # self.tabwidget.addTab(self.tab, "111")
        # self.tab1 = QtWidgets.QWidget()
        #
        # self.label = QtWidgets.QLabel(self.tab)
        # self.label.setObjectName("label")
        #
        #
        # # self.tab1.addWidget(self.tab1, 0, 1)
        # self.tab1.setObjectName("tab")
        # self.tabwidget.addTab(self.tab1, "111")
        # # self.tab2 = ContentTabWidget()
        # # self.tab2.setObjectName("tab2")
        # # self.tabwidget.addTab(self.tab2, "333")


        PDFReaderMainWindow.setCentralWidget(self.centralwidget)
        self.toolBarLeft = QtWidgets.QToolBar(PDFReaderMainWindow)  # ⭐ PDF 阅读工具栏
        self.toolBarLeft.setMinimumSize(QtCore.QSize(48, 0))
        self.toolBarLeft.setMaximumSize(QtCore.QSize(48, 16777215))
        self.toolBarLeft.setBaseSize(QtCore.QSize(40, 0))
        self.toolBarLeft.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.toolBarLeft.setAutoFillBackground(False)
        self.toolBarLeft.setMovable(False)
        self.toolBarLeft.setObjectName("toolBarLeft")
        PDFReaderMainWindow.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolBarLeft)
        self.toolBarRight = QtWidgets.QToolBar(PDFReaderMainWindow) # ⭐ markdown 辅助工具栏
        self.toolBarRight.setMinimumSize(QtCore.QSize(0, 0))
        self.toolBarRight.setMaximumSize(QtCore.QSize(48, 16777215))
        self.toolBarRight.setBaseSize(QtCore.QSize(48, 0))
        self.toolBarRight.setMovable(False)
        self.toolBarRight.setOrientation(QtCore.Qt.Vertical)
        self.toolBarRight.setObjectName("toolBarRight")
        PDFReaderMainWindow.addToolBar(QtCore.Qt.RightToolBarArea, self.toolBarRight)
        self.statusbar = QtWidgets.QStatusBar(PDFReaderMainWindow)
        self.statusbar.setObjectName("statusbar")
        PDFReaderMainWindow.setStatusBar(self.statusbar)
        self.menuBar = QtWidgets.QMenuBar(PDFReaderMainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 815, 23))
        self.menuBar.setMinimumSize(QtCore.QSize(0, 0))
        self.menuBar.setBaseSize(QtCore.QSize(0, 48))
        self.menuBar.setObjectName("menuBar")

        self.tab_list  = list()
        self.pdf_view_label_list = list()

        # 添加菜单项
        self.menuFileOperation = QtWidgets.QMenu(self.menuBar)
        self.menuHelpOperation = QtWidgets.QMenu(self.menuBar)
        self.menuFileOperation.setObjectName("menuFileOperation")
        self.menuHelpOperation.setObjectName("menuHelpOperation")

        PDFReaderMainWindow.setMenuBar(self.menuBar)
        self.actionShowProject = QtWidgets.QAction(PDFReaderMainWindow) # ⭐
        self.actionShowProject.setCheckable(True)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/svg/project.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off) # ⭐
        self.actionShowProject.setIcon(icon)
        self.actionShowProject.setObjectName("actionShowProject")
        self.actionClose = QtWidgets.QAction(PDFReaderMainWindow)
        self.actionClose.setCheckable(True)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/svg/close.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionClose.setIcon(icon1)
        self.actionClose.setObjectName("actionClose")
        # self.actionClose.triggered.connect(self.openFile)
        self.actionMinimize = QtWidgets.QAction(PDFReaderMainWindow)

        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/svg/minimize.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionMinimize.setIcon(icon2)
        self.actionMinimize.setObjectName("actionMinimize")
        self.actionMaximize = QtWidgets.QAction(PDFReaderMainWindow)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/svg/maximize-big.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionMaximize.setIcon(icon3)
        self.actionMaximize.setObjectName("actionMaximize")

        # 动作创建、命名、链接动作等
        ## menu 菜单动作
        ### “文件” menuFileOperation
        self.actionOpenFile = QtWidgets.QAction(PDFReaderMainWindow)
        self.actionOpenSettings = QtWidgets.QAction(PDFReaderMainWindow)
        self.actionOpenFile.setObjectName("actionOpenFile")
        self.actionOpenSettings.setObjectName("actionOpenSettings")
        self.actionOpenFile.triggered.connect(self.openFile)    # ⭐
        # self.actionOpenSettings.triggered.connect(self.openSettings)
        ### "帮助" menuHelpOperation
        self.actionShowAboutSoftware = QtWidgets.QAction(PDFReaderMainWindow)
        self.actionShowAboutSoftware.setObjectName("actionShowAboutSoftware")
        # self.actionShowAboutSoftware.triggered.connect(self.showAboutSoftware)

        self.toolBarLeft.addAction(self.actionShowProject)  # ⭐
        self.toolBarRight.addAction(self.actionShowProject) # ⭐
        # self.toolBarRight.addAction(self.actionClose)   # ⭐

        # 添加 action 到 menu
        ## “文件” menuFileOperation
        self.menuFileOperation.addAction(self.actionOpenFile)
        self.menuFileOperation.addAction(self.actionOpenSettings)
        ## “帮助” menuHelpOperation
        self.menuHelpOperation.addAction(self.actionShowAboutSoftware)


        # 添加 action 到 menuBar
        self.menuBar.addAction(self.menuFileOperation.menuAction())
        self.menuBar.addAction(self.menuHelpOperation.menuAction())


        self.retranslateUi(PDFReaderMainWindow)
        # self.showFeatureToolBarRight()
        # self.showFeatureToolBarLeft()
        # self.cancelFeatureToolBarRight()
        # self.cancelFeatureToolBarLeft()
        QtCore.QMetaObject.connectSlotsByName(PDFReaderMainWindow)


        self.loadPythonFeatures(PDFReaderMainWindow, self.actionFunc)  # ⭐ 加载插件到左边


    def changeActionLoadFunc(self, func):
        self.actionFunc = func

    # 实现打开文件功能 actionOpenFile
    def openFile(self):
        self.file_list = list()
        file, info = QFileDialog.getOpenFileName()
        self.file_list.append(file)
        # self.showPages(file)
        self.statusbar.showMessage(file)
        # self.addTab()
        print('open file')


    # 实现打开设置页面功能
    def openSettings(self):
        print("openSettings")

    # 显示“菜单栏->帮助->关于软件”的界面
    def showAboutSoftware(self):
        # from Ui_AboutSoftware import Ui_AboutSoftware as uia
        print("showAboutSoftware")
        #ui = Ui_AboutSoftware.Ui_AboutSoftware.setupUi()
        #self.showAboutSoftwareUi = uia()

        # self.widget = uia()
        # self.widget.show()


    def retranslateUi(self, PDFReaderMainWindow):
        _translate = QtCore.QCoreApplication.translate
        PDFReaderMainWindow.setWindowTitle(_translate("PDFReaderMainWindow", "PDFReader"))
        self.toolBarLeft.setWindowTitle(_translate("PDFReaderMainWindow", "ToolBarLeft"))
        self.toolBarRight.setWindowTitle(_translate("PDFReaderMainWindow", "ToolBarRight"))
        # menuBar
        self.menuFileOperation.setTitle(_translate("PDFReaderMainWindow", "文件"))
        self.menuHelpOperation.setTitle(_translate("PDFReaderMainWindow", "帮助"))

        # menu action
        ## menuFileOperation
        self.actionOpenFile.setText(_translate("PDFReaderMainWindow", "打开"))
        self.actionOpenSettings.setText(_translate("PDFReaderMainWindow", "设置"))
        ## menuHelpOperation
        self.actionShowAboutSoftware.setText(_translate("PDFReaderMainWindow", "关于"))

        self.actionShowProject.setText(_translate("PDFReaderMainWindow", "项目"))
        self.actionClose.setText(_translate("PDFReaderMainWindow", "关闭"))
        self.actionMinimize.setText(_translate("PDFReaderMainWindow", "最小化"))
        self.actionMaximize.setText(_translate("PDFReaderMainWindow", "最大化"))
        # self.label.setText(_translate("PDFReaderMainWindow", "demo"))


        # self.label.setText(_translate("PDFReaderMainWindow", "label"))

    # def addTab(self):
    #     self.tab = QtWidgets.QWidget()
    #     self.tabwidget.addTab(self.tab, "PDF")
    #     self.tab_list.append(self.tab)
    #     self.label = QtWidgets.QLabel(self.tab)
    #     self.scrollarea = QtWidgets.QScrollArea(self)
    #     self.scrollarea.setWidget(self.label)
    #
    #     self.pdf_view_label_list.append(self.label)


    def getDesktopSize(self):
        desktop_size = QDesktopWidget()
        return desktop_size.width(), desktop_size.height()
    def setBookPath(self, path):
        self._book_path = path

    def setBookName(self, bookname):
        self._book_name = bookname

    def showFeatureToolBarLeft(self):   # 测试用 pass
        self._left_bar = FeatureBarWidget()
        self._left_bar.addFeatureItem("Feature", self._right_bar.openFileAction)
        self._left_bar.addSpacer()
        self.main_widget_layout.insertWidget(0, self._left_bar)
        return

    def showFeatureToolBarLeftWithWidget(self, widget):   # 测试 ing
        self._left_bar = widget
        self._left_bar.setObjectName("LeftBar")

        # self._left_bar.addFeatureItem("Feature", self._right_bar.openFileAction)
        # self._left_bar.addSpacer()
        self.main_widget_layout.insertWidget(0, self._left_bar)
        self._left_bar.show()
        return

    def showFeatureToolBarRight(self):  # 测试用 pass
        self._right_bar = FeatureBarWidget()
        self._right_bar.addFeatureItem("Feature", self._right_bar.openFileAction)
        self._right_bar.addSpacer()
        self.main_widget_layout.insertWidget(3, self._right_bar)

    def cancelFeatureToolBarRight(self):    # 测试用 pass
        # for i in range(self.features_toolbar_right_layout.count()):
        #     self.features_toolbar_right_layout.itemAt(i).widget().deleteLater()
        # self.features_toolbar_right_widget.resize(0, 0)
        self._right_bar.deleteLater()
        # self.main_widget_layout.insertWidget(0, self.pdf_reader_widget)
        # self.main_widget_layout.insertWidget(1, self.mdnote_widget)
        self.main_widget_layout.update()



    def cancelFeatureToolBarLeft(self):
        # for i in range(self.features_toolbar_right_layout.count()):
        #     self.features_toolbar_right_layout.itemAt(i).widget().deleteLater()
        # self.features_toolbar_right_widget.resize(0, 0)
        # self._left_bar.deleteLater()
        self._left_bar.hide()
        self.main_widget_layout.update()




    def addFeatureToolBarLeft(self):    # 测试用
        # PDF 阅读工具栏
        self._right_bar = FeatureBarWidget()
        self._right_bar.addFeatureItem("Feature", self._right_bar.openFileAction)
        self._right_bar.addSpacer()
        self.main_widget_layout.insertWidget(3, self._right_bar)
        return

    def addFeatureToolBarRight(self):
        # markdown 写作工具栏

        return

    # 窗口大小更改事件处理函数
    def resize_event(self, event):
        self.main_widget_layout.invalidate()  # 使布局无效
        self.main_widget_layout.update()  # 更新布局并重新调整控件大小

    def getPythonPluginList(self):
        self._python_plugin_list = os.listdir(self._python_plugin_path)
        if '__init__.py' in self._python_plugin_list:
            self._python_plugin_list.remove('__init__.py')
        # print(self._python_plugin_list)
    def getJavaScriptPluginList(self):
        self._javascript_plugin_list = os.listdir(self._javascript_plugin_path)
        if '__init__.py' in self._javascript_plugin_list:
            self._javascript_plugin_list.remove('__init__.py')
        # print(self._javascript_plugin_list)

    def getPythonPluginInfoList(self):
        for plugin in self._python_plugin_list:
            config_path = os.path.join(self._python_plugin_path, plugin, "config.json")
            # print(config_path)
            with open(config_path, 'r', encoding="utf-8") as f:
                info_dict = json.load(f)
            # print(info_dict)

            self._python_plugin_info_list.append([plugin, info_dict])

    def getJavaScriptPluginInfoList(self):
        for plugin in self._javascript_plugin_list:
            config_path = os.path.join(self._javascript_plugin_path, plugin, "config.json")
            # print(config_path)
            with open(config_path, 'r', encoding="utf-8") as f:
                info_dict = json.load(f)
            # print(info_dict)

            self._javascript_plugin_info_list.append([plugin, info_dict])

    def loadPythonFeatures(self, PDFReaderMainWindow, func):
        for plugin in self._python_plugin_info_list:
            path = os.path.join(self._python_plugin_path, plugin[1]["dir"])
            sys.path.append(path)
            plugin_id = plugin[1]["id"]
            plugin_path = os.path.join(self._python_plugin_path, plugin[1]["path"])
            # print(path)
            # print(plugin_id)
            # print(plugin_path)
            py_class = plugin[1]["class"]
            # module = importlib.import_module(plugin_path)


            spec = importlib.util.spec_from_file_location('module', plugin_path)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            window = module.init_window()
            # self.showFeatureToolBarLeftWithWidget(window)   # 测试用，Python 插件加载热插拔完成，已 OK

            action = QtWidgets.QAction(PDFReaderMainWindow)
            action.setCheckable(True)
            action.setChecked(False)
            icon = QtGui.QIcon()
            icon_path = os.path.join(self._python_plugin_path, plugin[1]["icon"])
            icon.addPixmap(QtGui.QPixmap(icon_path), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            action.setIcon(icon)
            action.setObjectName(plugin[1]["id"])

            self._feature_list.append(action)
            self._actionInfoList.append((action, window))
            self.toolBarLeft.addAction(action)
            # action.triggered.connect(self.toggleActionTriggered)
            action.triggered.connect(func)
            # action.triggered.connect(self.openFile)
            print("Action")

    def getActions(self):
        return self._feature_list


            # self.showFeatureToolBarLeftWithWidget()

    def toggleActionTriggered(self, checked):
        action = self.sender()
        widget = self.getWidgetOfAction(action)
        # widget_clone = widget.clone()
        print(widget)
        if checked:
            # if self.findChild('LeftBar') is not None:
            self._left_bar.hide()
            self.setOtherActionOff(action)
            self.showFeatureToolBarLeftWithWidget(widget)
            # self._left_bar.show()
            # self.showFeatureToolBarLeft()
            print("选中")
        else:
            self.cancelFeatureToolBarLeft()
            print("未选中")


    def getWidgetOfAction(self, action):
        for item in self._actionInfoList:
            if item[0].objectName == action.objectName:
                return item[1]



    def setOtherActionOff(self, action):
        for item in self._actionInfoList:
            if item[0].objectName != action.objectName:
                item[0].setChecked(False)

    # # def actionController(self, ):
    #     for item in self._actionInfoList:
    #         if item[1].objectName ==



# import PDFReaderMainWindow_rc

from ui.uicomponents.mainfunc.PDFReaderMainWindowComponent import PDFReaderMainWindow_rc




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    Ui = Ui_PDFReaderMainWindow()
    # Ui.showFullScreen()
    Ui.resizeEvent = Ui.resize_event
    Ui.setBookName("sample")
    Ui.setBookPath("F:/sample.pdf")
    # Ui.loadFeaturesLeft()
    # MainWindow = QtWidgets.QWidget()
    # Ui = ContentTabWidget()
    Ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())