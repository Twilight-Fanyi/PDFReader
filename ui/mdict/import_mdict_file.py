import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QFileDialog, QStyle, QStyleFactory
from Ui_MainWindow import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        # 菜单的点击事件，当点击关闭菜单时连接槽函数 close()
        self.fileCLoseAction.triggered.connect(self.close)
        # 菜单的点击事件，当点击打开菜单时连接槽函数 openMsg()
        self.fileOpenAction.triggered.connect(self.openMsg)
        self.setWindowFlags(Qt.WindowStaysOnBottomHint)


    def openMsg(self):
        file, ok = QFileDialog.getOpenFileName(self, "打开", "C:/", "词典文件(*.mdx);;词典文件(*.mdd)")
        # 在状态栏显示文件地址
        self.statusbar.showMessage(file)



if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec())