import sys
import multiprocessing
from PyQt5 import QtCore, QtWidgets, QtWebEngineWidgets
from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineCore import QWebEngineUrlRequestInterceptor, QWebEngineUrlRequestJob
from flask import Flask, Response, send_file, redirect
import io
from waitress import serve

app = Flask(__name__)

o_pdf_path = 'F:/sample.pdf'

def changePdfPath(path):
    global o_pdf_path
    o_pdf_path = path

@app.route('/')
def pdf_view():
    return redirect('http://127.0.0.1:5000')

@app.route('/pdf')
def get_pdf():
    pdf_path = o_pdf_path
    pdf_data = convert_pdf_to_bytes(pdf_path)
    pdf_stream = io.BytesIO(pdf_data)
    headers = {
        "Content-Disposition": "inline; filename=sample.pdf",
        "Content-Type": "application/pdf",
    }
    return Response(pdf_stream.getvalue(), headers=headers)

def convert_pdf_to_bytes(file_path):
    with open(file_path, 'rb') as file:
        byte_stream = file.read()
    return byte_stream

def flask_run():
    serve(app, host='127.0.0.1', port=5000)

# class UrlRequestInterceptor(QWebEngineUrlRequestInterceptor):
#     def interceptRequest(self, info: QWebEngineUrlRequestJob):
#         headers = info.request().rawHeaderList()
#         headers.append(b"Access-Control-Allow-Origin: *")
#         info.request().setRawHeaderList(headers)
#         info.reply(info.request())
# class PdfUrlRequestInterceptor(QWebEngineUrlRequestInterceptor):
#     def interceptRequest(self, info):
#         url = info.requestUrl().toString()
#         if url.startswith('file://'):
#             info.redirect(QUrl(url))

class PDFReaderWidget(QtWebEngineWidgets.QWebEngineView):
    def __init__(self, path):
        super().__init__()
        self._pdf_path = path
        # self._url_path = 'http://127.0.0.1:5000/static/pdfjs/web/viewer.html'
        # self._url_path = 'www.baidu.com'
        self._url_path = 'http://127.0.0.1:5000/show'
        # self.settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.PluginsEnabled, True)
        # self.settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.PdfViewerEnabled, True)
        # changePdfPath(self._pdf_path)
        # self.settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.JavascriptEnabled, True)
        # self.settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.JavascriptCanOpenWindows, True)
        # self.settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.LocalContentCanAccessRemoteUrls, True)
        # self.settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.LocalContentCanAccessFileUrls, True)
        # self.settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.PluginsEnabled, True)
        # self.settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.WebGLEnabled, True)
        # self.settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.WebRTCPublicInterfacesOnly, True)



    def load_pdf(self):
        self.load(QtCore.QUrl.fromUserInput(self._url_path))
        self.loadFinished.connect(self.handle_load_finished)

    def handle_load_finished(self, ok):
        if ok:
            # Page loaded successfully
            print('Page loaded successfully')
        else:
            # Page failed to load
            print('Page failed to load')
            print(self.page().contentStatus())

            # Print JavaScript errors
            for message in self.page().messages(
                    QtWebEngineWidgets.QWebEnginePage.JavascriptConsoleMessageLevel):
                print(message.message())

if __name__ == '__main__':
    # app_process = multiprocessing.Process(target=flask_run)
    # app_process.start()
    # app_process.join()

    app = QtWidgets.QApplication(['', '--no-sandbox'])
    window = PDFReaderWidget("F:/sample.pdf")
    window.load_pdf()
    window.show()

    sys.exit(app.exec_())
