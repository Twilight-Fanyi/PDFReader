import sys
from PyQt5 import QtCore, QtWidgets, QtWebEngineWidgets

# PDF = 'E:/Projects/QT/PDFReader/util/pdfreader/sample.pdf'

class PDFReaderWidget(QtWebEngineWidgets.QWebEngineView):
    def __init__(self, path):
        super().__init__()
        self._pdf_path = path
        self.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.PluginsEnabled, True)
        self.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.PdfViewerEnabled, True)
        # self.load(QtCore.QUrl.fromUserInput(PDF))
        self.load_pdf()
        self.autoFillBackground()


    def load_pdf(self):
        self.load(QtCore.QUrl.fromUserInput(self._pdf_path))



if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)
    window = PDFReaderWidget("F:/sample.pdf")
    window.setGeometry(600, 50, 800, 600)
    window.show()
    sys.exit(app.exec_())