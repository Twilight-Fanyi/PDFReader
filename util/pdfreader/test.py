from PyQt5.QtWidgets import QWidget, QApplication, QVBoxLayout
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5 import QtCore
app = QApplication(['', '--no-sandbox'])
main = QWebEngineView()
main.load(QtCore.QUrl(f"http://127.0.0.1:5000/static/pdfjs/web/viewer.html"))
main.show()
app.exec_()