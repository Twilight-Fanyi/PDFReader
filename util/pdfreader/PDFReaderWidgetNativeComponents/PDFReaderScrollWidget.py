from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QScrollArea
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPdfWriter


class PDFReaderScrollWidget(QScrollArea):

    def __init__(self, path):
        super().__init__()
        self._pdf_path = path
        self.setObjectName("PDFReaderScrollWidget")

        self.content_widget = QWidget()
        self.content_widget.setObjectName("PDFReaderScrollWidgetContent")

        self.setWidget(self.content_widget)



if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)

    UI = PDFReaderScrollWidget("F:/sample.pdf")
    UI.show()

    sys.exit(app.exec_())


