import io

from flask import Flask, Response, send_file, redirect

app = Flask(__name__)

o_pdf_path = 'F:/sample.pdf'
def changePdfPath(path):
    o_pdf_path = path

@app.route('/')
def pdf_view():
    return redirect('http://127.0.0.1:5000/static/pdfjs/web/viewer.html')
@app.route('/pdf')
def get_pdf():
    pdf_path = o_pdf_path
    pdf_data = convert_pdf_to_bytes(pdf_path)
    # print(pdf_data)
    pdf_stream = io.BytesIO(pdf_data)
    headers = {
        "Content_Disposition": "inline; filename=sample.pdf",
        "Content-Type": "application/pdf",
    }

    return Response(pdf_stream.getvalue(), headers=headers)

def convert_pdf_to_bytes(file_path):
    with open(file_path, 'rb') as file:
        byte_stream = file.read()
    return byte_stream

def APP_RUN():
    app.run(debug=True)

if __name__ == '__main__':
    APP_RUN()