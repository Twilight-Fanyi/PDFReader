import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QScrollArea, QLabel, QPushButton, QSpacerItem, QSizePolicy
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt
import fitz

class PDFReaderWidgetWithNative (QWidget):
    def __init__(self, filename):
        super().__init__()
        self.setWindowTitle('PDF Reader')
        self.setMaximumWidth(900)
        # self.setGeometry(100, 100, 800, 600)
        self.adjustSize()
        self.pdf_path = filename
        self.pdf_pages = []  # 存储PDF页面的QPixmap对象
        self.current_page = 0
        self.zoom_factor = 1.0

        self.scroll_area = QScrollArea()
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)

        self.scroll_content = QWidget(self.scroll_area)
        self.scroll_layout = QVBoxLayout(self.scroll_content)
        self.scroll_layout.setAlignment(Qt.AlignCenter)

        self.pdf_content = QWidget()
        self.pdf_content.setObjectName("PDFContent")
        self.pdf_content_layout = QVBoxLayout(self.pdf_content)
        self.pdf_content_layout.setAlignment(Qt.AlignTop)

        self.scroll_layout.addWidget(self.pdf_content)

        self.scroll_area.setWidget(self.scroll_content)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.scroll_area)

        self.zoom_in_button = QPushButton("放大")
        self.zoom_in_button.clicked.connect(self.zoom_in)
        self.layout.addWidget(self.zoom_in_button)

        self.zoom_out_button = QPushButton("缩小")
        self.zoom_out_button.clicked.connect(self.zoom_out)
        self.layout.addWidget(self.zoom_out_button)

        self.setLayout(self.layout)
        # central_widget = QWidget()
        # central_widget.setLayout(layout)
        # self.setCentralWidget(central_widget)

        self.load_pdf(filename)
        # self.setStyleSheet("#PDFContent{background:rgb(0,249,251);}")
        self.setStyleSheet('''
                    QScrollBar:vertical {
                        background-color: #f0f0f0;
                        width: 3px;
                        margin: 0px;
                    }

                    QScrollBar::handle:vertical {
                        background-color: #c0c0c0;
                        min-height: 20px;
                    }

                    QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {
                        background-color: #f0f0f0;
                    }

                    QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {
                        background-color: #f0f0f0;
                        border: none;
                        width: 15px;
                        height: 15px;
                        subcontrol-position: top;
                        subcontrol-origin: margin;
                    }
                ''')
        self.adjustSize()


    def load_pdf(self, filename):
        doc = fitz.open(self.pdf_path)
        for i in range(doc.page_count):
            page = doc.load_page(i)
            pixmap = self.get_page_pixmap(page)
            self.pdf_pages.append(pixmap)

        self.display_pages()
        # print(self.pdf_content.sizeHint())
        # self.pdf_content.setFixedWidth(614)
        self.pdf_content.adjustSize()
        self.adjust_scroll_area_size()

    def get_page_pixmap(self, page):
        mat = fitz.Matrix(self.zoom_factor, self.zoom_factor)  # 应用缩放因子
        print(mat)
        pix = page.get_pixmap(matrix=mat)
        print(pix)
        img = QImage(pix.samples, pix.width, pix.height, pix.stride, QImage.Format_RGB888).copy()
        pixmap = QPixmap.fromImage(img)
        return pixmap

    # def get_page_pixmap(self, page):
    #     mat = fitz.Matrix(self.zoom_factor, self.zoom_factor)  # 应用缩放因子
    #     pix = page.get_pixmap(matrix=mat)
    #     img = QImage(pix.samples, pix.width, pix.height, pix.stride, QImage.Format_RGB888).copy()
    #
    #     # 调整图像的高度
    #     # scaled_height = int(pix.height * self.zoom_factor)
    #     # img = img.scaledToHeight(scaled_height, Qt.SmoothTransformation)
    #
    #     pixmap = QPixmap.fromImage(img)
    #     return pixmap

    def display_pages(self):
        for pixmap in self.pdf_pages:
            label = QLabel()
            label.setPixmap(pixmap)
            label.setMargin(1)
            label.setScaledContents(True)
            # label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
            # label.setFixedSize(pixmap.width(), pixmap.height())
            # label.setAutoFillBackground(False)

            self.pdf_content_layout.addWidget(label)

        # 添加顶部和底部的占位小部件，创建空白空间
        spacer = QSpacerItem(1, 1, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.pdf_content_layout.addSpacerItem(spacer)

    def scroll_to_previous_page(self):
        if self.current_page > 0:
            self.current_page -= 1
            self.scroll_area.ensureWidgetVisible(self.pdf_content_layout.itemAt(self.current_page).widget())

    def scroll_to_next_page(self):
        if self.current_page < len(self.pdf_pages) - 1:
            self.current_page += 1
            self.scroll_area.ensureWidgetVisible(self.pdf_content_layout.itemAt(self.current_page).widget())

    def zoom_in(self):
        self.zoom_factor += 0.1
        self.update_zoom()

    def zoom_out(self):
        if self.zoom_factor > 0.1:
            self.zoom_factor -= 0.1
            self.update_zoom()

    # def update_zoom(self):
    #     pixmap = self.get_page_pixmap(self.pdf_pages[self.current_page])
    #     new_label = QLabel()
    #     new_label.setPixmap(pixmap)
    #
    #     old_widget = self.pdf_content_layout.itemAt(self.current_page).widget()
    #     self.pdf_content_layout.replaceWidget(old_widget, new_label)
    #     old_widget.deleteLater()
    #
    #     # self.pdf_content_layout.insertWidget(self.current_page, new_label)
    #
    #     self.adjust_scroll_area_size()
    # def update_zoom(self):
    #     # self.zoom_factor += 0.2
    #     # if self.zoom_factor > 2.0:  # 设置最大缩放倍数为 2.0
    #     #     self.zoom_factor = 2.0
    #     doc = fitz.open(self.pdf_path)
    #     page = doc.load_page(self.current_page)
    #     # page = self.pdf_pages[self.current_page]
    #     print(page)
    #     pixmap = self.get_page_pixmap(page)
    #     print(pixmap)
    #     label = self.pdf_content_layout.itemAt(self.current_page).widget()
    #     label.setPixmap(pixmap)

        # self.adjust_scroll_area_size()

    def update_zoom(self):
        doc = fitz.open(self.pdf_path)
        for i in range(len(self.pdf_pages)):
            page = doc.load_page(i)
            pixmap = self.get_page_pixmap(page)
            label = self.pdf_content_layout.itemAt(i).widget()
            label.setPixmap(pixmap)

        self.adjust_scroll_area_size()

    # def update_zoom(self):
    #     start_page = max(0, self.current_page - 2)
    #     end_page = min(len(self.pdf_pages) - 1, self.current_page + 2)
    #     doc = fitz.open(self.pdf_path)
    #     for i in range(start_page, end_page + 1):
    #         page = doc.load_page(i)
    #         pixmap = self.get_page_pixmap(page)
    #         label = self.pdf_content_layout.itemAt(i).widget()
    #         label.setPixmap(pixmap)
    #
    #     self.adjust_scroll_area_size()

    def adjust_scroll_area_size(self):
        content_height = 0
        for i in range(self.pdf_content_layout.count()):
            item = self.pdf_content_layout.itemAt(i)
            if item is not None and item.widget() is not None:
                content_height += item.widget().pixmap().height()

        # self.scroll_content.setMinimumHeight(content_height)

        # 调整滚动区域的滚动位置，使当前页面居中可见
        page_widget = self.pdf_content_layout.itemAt(self.current_page).widget()
        scroll_pos = page_widget.y() - self.scroll_area.height() // 2 + page_widget.height() // 2
        self.scroll_area.verticalScrollBar().setValue(scroll_pos)

    # def adjust_scroll_area_size(self):
    #     viewport_rect = self.scroll_area.viewport().rect()
    #     top = max(0, self.pdf_content_layout.count() - viewport_rect.height())
    #     bottom = min(top + viewport_rect.height(), self.pdf_content_layout.count())
    #
    #     content_height = 0
    #     for i in range(top, bottom):
    #         item = self.pdf_content_layout.itemAt(i)
    #         if item is not None and item.widget() is not None:
    #             pixmap = item.widget().pixmap()
    #             content_height += pixmap.height()
    #
    #     # self.scroll_content.setMinimumHeight(content_height)
    #
    #     # 调整滚动区域的滚动位置，使当前页面居中可见
    #     page_widget = self.pdf_content_layout.itemAt(self.current_page).widget()
    #     scroll_pos = page_widget.y() - self.scroll_area.height() // 2 + page_widget.height() // 2
    #     self.scroll_area.verticalScrollBar().setValue(scroll_pos)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    reader = PDFReaderWidgetWithNative('sample.pdf')
    reader.show()
    sys.exit(app.exec_())