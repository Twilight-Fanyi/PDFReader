import sys
import multiprocessing
from PyQt5 import QtCore, QtWidgets, QtWebEngineWidgets
from flask import Flask, Response, send_file, redirect
import io

app = Flask(__name__)

o_pdf_path = 'F:/sample.pdf'

def changePdfPath(path):
    global o_pdf_path
    o_pdf_path = path

@app.route('/')
def pdf_view():
    return redirect('http://127.0.0.1:5000/static/pdfjs/web/viewer.html')

@app.route('/pdf')
def get_pdf():
    pdf_path = o_pdf_path
    pdf_data = convert_pdf_to_bytes(pdf_path)
    pdf_stream = io.BytesIO(pdf_data)
    headers = {
        "Content-Disposition": "inline; filename=sample.pdf",
        "Content-Type": "application/pdf",
    }
    return Response(pdf_stream.getvalue(), headers=headers)

def convert_pdf_to_bytes(file_path):
    with open(file_path, 'rb') as file:
        byte_stream = file.read()
    return byte_stream

def flask_run():
    app.run(debug=True)

class PDFReaderWidget(QtWebEngineWidgets.QWebEngineView):
    def __init__(self, path):
        super().__init__()
        self._pdf_path = path
        self._url_path = 'http://127.0.0.1:5000/static/pdfjs/web/viewer.html'
        self.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.PluginsEnabled, True)
        self.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.PdfViewerEnabled, True)
        changePdfPath(self._pdf_path)

    def load_pdf(self):
        self.load(QtCore.QUrl.fromUserInput(self._url_path))


if __name__ == '__main__':
    app_process = multiprocessing.Process(target=flask_run)
    app_process.start()

    app = QtWidgets.QApplication(sys.argv)
    window = PDFReaderWidget("F:/sample.pdf")
    window.load_pdf()
    window.show()

    sys.exit(app.exec_())
