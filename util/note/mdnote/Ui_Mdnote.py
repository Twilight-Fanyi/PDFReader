# -*- coding: utf-8 -*-
import os
import re
from time import sleep

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import QtWebEngineWidgets
from PyQt5.QtCore import QUrl, QJsonValue, pyqtSlot
from PyQt5.QtWebChannel import QWebChannel
from PyQt5.QtWidgets import QMessageBox


class Ui_Mdnote(QtWebEngineWidgets.QWebEngineView):
    # class Ui_Mdnote(QtWidgets.QWidget):

    def __init__(self, path, bookname, parent=None):
        super(Ui_Mdnote, self).__init__(parent)
        # self.main_widget_layout = QtWidgets.QGridLayout()
        # self.setLayout(self.main_widget_layout)
        # self.webview = QtWebEngineWidgets.QWebEngineView()
        self.setObjectName("MdEditorWebView")
        # self.main_widget_layout.addWidget(self.webview)

        self.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.LocalContentCanAccessFileUrls, True
        )
        self.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.LocalStorageEnabled, True
        )

        self.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.LocalContentCanAccessRemoteUrls, True
        )

        self.settings().setAttribute(
            QtWebEngineWidgets.QWebEngineSettings.WebAttribute.JavascriptEnabled, True
        )

        self._book_path = path
        self._book_name = bookname
        self._files_path = self._book_path.replace(self._book_name + '.pdf', '')
        self._md_note_path = self._files_path + self._book_name + '.md'
        # os.path.join(os.getcwd(), "mdeditor", "../mdeditor/index.html")
        # self.url = os.path.join(os.getcwd(), "mdeditor", "../mdeditor/index.html")
        self.url = os.path.join(os.path.expanduser("~"), "PDFReader\\jscomponent\\mdeditor\\index.html")
        print(self.url)

        self.channel = QWebChannel()
        self.page().setWebChannel(self.channel)
        self.channel.registerObject("pythonBridge", self)
        # self.settings().globalSettings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.JavascriptEnabled, True
        # )
        # self.page().settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.JavascriptEnabled, True
        # )
        # self.page().settings().setAttribute(
        #     QtWebEngineWidgets.QWebEngineSettings.WebAttribute.AllowWindowActivationFromJavaScript, True
        # )
        self.load(QUrl.fromLocalFile(self.url))
        self.loadMarkdownNote()

    def resetUrl(self, url):
        self.url = url
        self.load(QUrl.fromLocalFile(self.url))

    def loadMarkdownNote(self):

        note_name = '【笔记】' + self._book_name
        note_path = os.path.join(self._md_note_path)

        if not os.path.exists(self._md_note_path):
            with open(self._md_note_path, 'w+', encoding="utf-8") as md_note:
                md_note.close()
        # print(self._md_note_path)

        with open(note_path, 'r', encoding="utf8") as f:
            note_content = f.read()

        data = {"note_name": note_name, "note_content": note_content}
        print(data)

        # 什么鬼东西，找了一天终于找到了解决方法，loadFinished 在页面加载完成后再执行此方法，国内一点都找不到
        # (๑′ᴗ‵๑)Ｉ Lᵒᵛᵉᵧₒᵤ❤
        # self.page().loadFinished.connect(lambda: self.page().runJavaScript("setMarkdownTitle({0});".format(note_name)))
        # self.page().loadFinished.connect(lambda:self.page().runJavaScript("setMarkdown({0});".format(note_content)))
        self.page().loadFinished.connect(lambda: self.page().runJavaScript("setTitleAndMarkdown({0});".format(data)))
        # self.page().loadFinished.connect(lambda: self.page().runJavaScript("setTitleAndMarkdown({0});".format(data)))
        # self.page().runJavaScript("setTitleAndMarkdown({0});".format(data))
        # self.page().loadFinished.connect(lambda: self.page().runJavaScript("setTitleAndMarkdown({0});".format(data)))

        # print("setTitleAndMarkdown({0});".format(data))
        # self.webview.page().runJavaScript("setTitleAndMarkdown({0});".format(data))
        # self.page().runJavaScript("setTitleAndMarkdown({'note_name': '4', 'note_content': 'aaaaaaaaaaaa\n\n\n\naaaaaaaa\n\n\n\n\n\n\n\n\n\n\n\n'});")
        # self.page().runJavaScript("setTitleAndMarkdown(data);")
        # self.webview.page().runJavaScript("alert('111');")
        # self.page().runJavaScript("customFunction();")
        # self.page().runJavaScript("$('#myDiv').text('Hello, jQuery!');")

        # print(self.webview.page().runJavaScript("setTitleAndMarkdown({0});".format(data)))
    #
    @pyqtSlot(QJsonValue)
    def save_note(self, note):
        # print('saveNote')
        note_title = note["title"].toString()
        note_content = note["content"].toString()
        print(note_title)
        print(note_content)
        # 当前笔记本
        # notebook = self._files_path
        # 当前笔记
        old_note = self._md_note_path

        if old_note is not None:
            if note_title is None:
                QMessageBox.information(
                    self, "提示", "标题不能为空", QMessageBox.Yes, QMessageBox.Yes)

            old_note_file = os.path.join(self._files_path, (self._book_name + ".md"))
            new_note_file = os.path.join(self._files_path, (note_title + ".md"))
            # 写入内容
        with open(self._md_note_path, "w", encoding="utf8") as f:
            f.write(note_content)

    # def setupUi(self, mainWindow):
    #     mainWindow.setObjectName("mainWindow")
    #     mainWindow.resize(1366, 768)
    #     self.centralwidget = QtWidgets.QWidget(mainWindow)
    #     self.mdEditorWbrowser = QtWebEngineWidgets.QWebEngineView(self.centralwidget)
    #     sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
    #     sizePolicy.setHorizontalStretch(0)
    #     sizePolicy.setVerticalStretch(0)
    #     sizePolicy.setHeightForWidth(self.mdEditorWbrowser.sizePolicy().hasHeightForWidth())
    #     self.mdEditorWbrowser.setSizePolicy(sizePolicy)
    #     self.mdEditorWbrowser.setObjectName("mdEditorWbrowser")
    #     mainWindow.setCentralWidget(self.centralwidget)
    #     self.retranslateUi(mainWindow)
    #     self.load_mdeditor()
    #     QtCore.QMetaObject.connectSlotsByName(mainWindow)

    # def retranslateUi(self, mainWindow):
    #     _translate = QtCore.QCoreApplication.translate
    #     mainWindow.setWindowTitle(_translate("mainWindow", "PyQtNote"))

    # def load_mdeditor(self):
    #     url = os.path.join(os.getcwd(), "mdeditor", "../mdeditor/index.html")
    #
    #     print(url)
    #     self.mdEditorWbrowser.load(QUrl.fromLocalFile(url))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = Ui_Mdnote('F:/sample.pdf', 'sample')
    MainWindow.show()

    sys.exit(app.exec_())
