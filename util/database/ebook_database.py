import os
import sqlite3
import time
import shutil

class EbookDatabase:
    def __init__(self):
        self.ebook_db_name = "ebookinfo.db"
        self.ebook_table = 'ebooktable'
        self.ebook_db_path = os.path.join(os.path.expanduser("~/PDFReader/database"), self.ebook_db_name)

    def init_db(self):

        conn = sqlite3.connect(self.ebook_db_path)
        cursor = conn.cursor()

        try:
            cursor.execute("create table %s (bookname text primary key, latesttime float, bookpath text)" % self.ebook_table)
            print("创建成功")
        except:
            print("创建失败")
            pass
        conn.commit()
        conn.close()
        shutil.copy("ebookinfo.db", self.ebook_db_path)


    #
    # def __init__(self, path):
    #     self.ebook_db_path = path
    #     self.ebook_db_name = "ebookinfo.db"
    #     self.ebook_table = 'ebooktable'
    #     conn = sqlite3.connect(self.ebook_db_name)
    #     cursor = conn.cursor()
    #
    #     try:
    #         cursor.execute("create table %s (bookname text primary key, latesttime float, bookpath text)" % self.ebook_table)
    #         print("创建成功")
    #     except:
    #         print("创建失败")
    #         pass
    #     conn.commit()
    #     conn.close()
    def update_book_latest_time(self, bookname, latesttime):

        conn = sqlite3.connect(self.ebook_db_path)
        cursor = conn.cursor()

        try:
            cursor.execute("update %s set latesttime %s where bookname = '%s'" % (self.ebook_table, latesttime, bookname))
            print("修改成功")
        except:
            print("修改失败")
            pass

        conn.commit()
        conn.close()


    def insert_book_info(self, bookname, latesttime, bookpath):

        conn = sqlite3.connect(self.ebook_db_path)
        cursor = conn.cursor()

        try:
            cursor.execute("insert into %s values('%s', %s, '%s')" % (self.ebook_table, bookname, latesttime, bookpath))
            print("修改成功")
        except:
            print("修改失败")
            pass

        conn.commit()
        conn.close()

    ## TODO: remove book_info function should be finished

    def select_book_info(self):
        conn = conn = sqlite3.connect(self.ebook_db_path)
        cursor = conn.cursor()

        try:
            info = cursor.execute("SELECT bookname, latesttime, bookpath FROM ebooktable")
            book_info = info.fetchall()
        except:
            book_info = 'failed'
            print("查找失败")
        return book_info



if __name__ == "__main__":
    database = EbookDatabase()
    database.insert_book_info('aaaa', 22222222222, 'test')
    info = database.select_book_info()
    print(len(info))
    for i in info:
        print(i)