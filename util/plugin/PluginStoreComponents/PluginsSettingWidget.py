import json
import os
import sys

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import Qt, QPropertyAnimation, QRect, pyqtProperty, pyqtSignal, QStringListModel
from PyQt5.QtGui import QPainter, QColor, QPen, QBrush, QPixmap
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QFileDialog, QAction, QMessageBox
# from PluginsSettingItem import PluginsSettingItem
from util.plugin.PluginStoreComponents.PluginStoreWidgetItem import PluginStoreWidgetItem

from util.database.ebook_database import EbookDatabase


from util.parsers.pdf_parser import PDF_Parser
from util.plugin.PluginStoreComponents.PluginsSettingItem import PluginsSettingItem

"""
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐

⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
"""



class PluginsSettingWidget(QWidget):

    # my_signal = pyqtSignal(int, result=int)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setObjectName("PluginStoreWidget")
        self.setFixedSize(900, 650)
        self.setContentsMargins(0, 0, 0, 0)
        self._book_number = 20
        self._number_of_each_row = 4
        self.plugin_info_list = []

        self.debug_mode = True

        self.temp_usage_single = 0

        self.plugin_path = os.path.join(os.path.expanduser("~"), "PDFReader", "plugins")
        self._python_plugin_path = os.path.join(self.plugin_path, "py_plugin")
        self._javascript_plugin_path = os.path.join(self.plugin_path, "js_plugin")
        print(self.plugin_path)
        print(self._python_plugin_path)
        print(self._javascript_plugin_path)

        if not os.path.exists(self.plugin_path):
            os.makedirs(self.plugin_path)
        if not os.path.exists(self._python_plugin_path):
            os.makedirs(self._python_plugin_path)
        if not os.path.exists(self._javascript_plugin_path):
            os.makedirs(self._javascript_plugin_path)


        # self._python_plugin_path = "../py_plugin"
        # self._javascript_plugin_path = "../js_plugin"


        self._python_plugin_list = []
        self._javascript_plugin_list = []

        self._python_plugin_info_list = []
        self._javascript_plugin_info_list = []

        self._item_1_name = "打开"
        self._item_2_name = "导入"

        self.setupUi()
        self.setStyleSheet("#PluginsSettingItem{"
                           "background:rgb(255,255,255);"
                           "font-size:15px;"
                           "font-family:Century Gothic,sans-serif;"
                           "border: 1px solid black;"  
                           "}")

        self.getPythonPluginList()
        self.getJavaScriptPluginList()

        self.getPythonPluginInfoList()
        self.getJavaScriptPluginInfoList()

        self.loadPluginStoreItem()
        self.addSpacer()

        # self.setStyleSheet("*{font-size:20px;"
        #                    "font-family:SimHei,sans-serif;"
        #                    "border-style:dotted dashed solid double"
        #                    "}"
        #                    "QPushButton{"
        #                    "background:rgb(0,255,255)"
        #                    "}")


        # self.setFirstPageAsCover()

    # TODO: content 布局更改为网格状
    def setupUi(self):
        self.main_widget_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.main_widget_layout)
        # self.main_widget_layout.setDirection(QtWidgets.QVBoxLayout.Direction.TopToBottom)
        # self.main_widget_layout.setContentsMargins(0, 0, 0, 0)
        self.scroll_roll_area_widget = QtWidgets.QScrollArea()
        self.content_widget = QtWidgets.QWidget()
        self.main_widget_layout.addWidget(self.scroll_roll_area_widget)
        self.content_widget = QtWidgets.QWidget()
        self.scroll_layout = QtWidgets.QVBoxLayout(self.content_widget)
        # self.scroll_layout.setColumnStretch(3, 1)

        self.scroll_roll_area_widget.setWidget(self.content_widget)
        self.scroll_roll_area_widget.setWidgetResizable(True)   # ⭐设置窗口小部件可调整大小
        self.main_widget_layout.addWidget(self.scroll_roll_area_widget)


    ## 测试用：向 QScrollArea 添加 Item Pass
    def addItemTest(self):
        for i in range(100):
            label = QtWidgets.QLabel(f"Label {i}")
            self.scroll_layout.addWidget(label)

    def getWidgetWidth(self):
        print(self.width())
        return self.width()

    ## TODO: 已废弃，不再使用
    def addPluginStoreWidget(self):
        for i in range(self._book_number):
            plugin_bar_widget = PluginsSettingItem()
            plugin_bar_widget.item_changed_signal.connect(self.changeItemStateAfterReceivingSignal)
            self.scroll_layout.addWidget(plugin_bar_widget)

    # TODO: get the number of book from DB
    # def addBookCoverWidgetFromDB(self):
    #     ebook_database = EbookDatabase()
    #     book_info = ebook_database.select_book_info()
    #     self._book_number = len(book_info)
    #     for i in range(self._book_number):
    #         book_cover_widget = PluginsSettingItem()
    #         book_name = book_info[i][0]
    #         book_path = book_info[i][2]
    #         print(book_path)
    #         print(book_name)
    #         book_cover_widget.setBookName(book_name)
    #         book_cover_widget.setFirstPageAsCoverFromPath(book_path)
    #         self.scroll_layout.addWidget(book_cover_widget,  i // self._number_of_each_row, i % self._number_of_each_row)

    def getPythonPluginList(self):
        self._python_plugin_list = os.listdir(self._python_plugin_path)
        if '__init__.py' in self._python_plugin_list:
            self._python_plugin_list.remove('__init__.py')
        print(self._python_plugin_list)

    def getJavaScriptPluginList(self):
        self._javascript_plugin_list = os.listdir(self._javascript_plugin_path)
        if '__init__.py' in self._javascript_plugin_list:
            self._javascript_plugin_list.remove('__init__.py')
        print(self._javascript_plugin_list)

    def getPythonPluginInfoList(self):
        for plugin in self._python_plugin_list:
            config_path = os.path.join(self._python_plugin_path, plugin, "config.json")
            # print(config_path)
            with open(config_path, 'r', encoding="utf-8") as f:
                info_dict = json.load(f)
            # print(info_dict)

            self._python_plugin_info_list.append([plugin, info_dict])

            # print(self._python_plugin_info_list)

    def getJavaScriptPluginInfoList(self):
        for plugin in self._javascript_plugin_list:
            config_path = os.path.join(self._javascript_plugin_path, plugin, "config.json")
            # print(config_path)
            with open(config_path, 'r', encoding="utf-8") as f:
                info_dict = json.load(f)
            # print(info_dict)

            self._javascript_plugin_info_list.append({plugin: info_dict})

            # print(self._javascript_plugin_info_list)

    def addPluginStoreItem(self, plugin_name, plugin_introduction):
        plugin_store_widget_item = PluginsSettingItem()
        plugin_store_widget_item.setPluginNameText(plugin_name)
        plugin_store_widget_item.setPluginIntroductionText(plugin_introduction)
        self.scroll_layout.addWidget(plugin_store_widget_item)


    # def loadPluginStoreItem(self):  # 根据读取的 config.json 所组成的列表加载插件
    #
    #     for info in self._python_plugin_info_list:
    #         print(info[1])
    #         print(info[1]['name'])
    #         print(info[1]['introduction'])
    #         self.addPluginStoreItem(info[1]['name'], info[1]['introduction'])

    def loadPluginStoreItem(self):
        for info in self._python_plugin_info_list:
            plugin_store_widget_item = PluginsSettingItem()
            plugin_store_widget_item.setID(info[1]['id'])
            plugin_store_widget_item.setPluginNameText(info[1]['name'])
            plugin_store_widget_item.setPluginIntroductionText(info[1]['introduction'])
            plugin_store_widget_item.item_changed_signal.connect(self.changeItemStateAfterReceivingSignal)  # 连接信号与槽函数
            self.scroll_layout.addWidget(plugin_store_widget_item)


    def changePluginConfigJSONFileAboutUsage(self, id, usage):
        print("Changing plugin config")
        try:
            for plugin in self._python_plugin_list:
                print(plugin)
                if plugin == id:
                    config_path = os.path.join(self._python_plugin_path, plugin, "config.json")
                    print(config_path)
                # print(config_path)
                    with open(config_path, 'r', encoding="utf-8") as f:
                        info_dict = json.load(f)

                    info_dict['usage'] = usage

                    with open(config_path, 'w', encoding='utf-8') as f:
                        json.dump(info_dict, f, ensure_ascii=False)
        except:
            if self.debug_mode:
                print("plugin not exist")
                messagebox = QtWidgets.QMessageBox().information("插件 config.json 配置文件出错，请检查后重试", QtWidgets.QMessageBox.Yes)
                messagebox.show()
            else:
                messagebox = QtWidgets.QMessageBox()
                messagebox.information("插件出错，请卸载该插件后重试。", QtWidgets.QMessageBox.Yes)
                messagebox.show()



            # print(info_dict)

            # self._python_plugin_info_list.append([plugin, info_dict])

    def setSettingItemState(self):
        pass


    def addSpacer(self):
        self.spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum,
                                                QtWidgets.QSizePolicy.Expanding)
        self.scroll_layout.addSpacerItem(self.spacerItem)




    # ⭐用来接收 PluginsSettingItem 中 setItemStateSameAsButtonState 的信号
    def changeItemStateAfterReceivingSignal(self, signal):
        print(signal)
        id = signal[0]
        usage = signal[1]
        self.changePluginConfigJSONFileAboutUsage(id, usage)    # ⭐



if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = PluginsSettingWidget()
    # UI.addFeatureItem("Feature", UI.openFileAction)
    # UI.addPluginStoreWidget()
    # # UI.getPythonPluginList()
    # # UI.getJavaScriptPluginList()
    # UI.getPythonPluginInfoList()
    # UI.getJavaScriptPluginInfoList()

    UI.show()
    sys.exit(app.exec_())
