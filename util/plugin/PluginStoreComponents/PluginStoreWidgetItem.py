from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget


class PluginStoreWidgetItem(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setObjectName("PluginStoreWidgetItem")
        self.setFixedHeight(100)
        self.plugin_install_button_text = "安装"
        self.plugin_name_text = "插件"
        self.plugin_introduction_text = "插件介绍"

        self.setupUi()
        self.setStyleSheet("#PluginStoreWidgetItem{\n"
"    background:rgb(255,255,255);\n"
"    font-size:15px;\n"
"    font-family:Century Gothic,sans-serif;\n"
"    border: 1px solid black;\n"  
"}\n"
"#PluginName{\n"
"    font-size:20px;\n"
"    font-weight: bold\n"
"}\n"
"#PluginIntroduction{\n"
"color:rgb(132,133,133);\n"
"}\n")

    def setupUi(self):
        self.main_widget_layout = QtWidgets.QHBoxLayout()
        self.setLayout(self.main_widget_layout)
        self.plugin_info = QtWidgets.QWidget()
        self.setObjectName("PluginInfo")
        self.plugin_info_layout = QtWidgets.QVBoxLayout()
        self.plugin_info.setLayout(self.plugin_info_layout)

        self.plugin_name = QtWidgets.QLabel()   # ⭐ 插件名称 Label
        self.plugin_name.setObjectName("PluginName")
        self.plugin_name.setText(self.plugin_name_text)

        self.plugin_introduction = QtWidgets.QLabel()   # ⭐ 插件介绍 Label
        self.plugin_introduction.setObjectName("PluginIntroduction")
        self.plugin_introduction.setText(self.plugin_introduction_text)

        self.plugin_info_layout.addWidget(self.plugin_name)
        self.plugin_info_layout.addWidget(self.plugin_introduction)

        self.plugin_install_button = QtWidgets.QPushButton()    # ⭐ 安装按钮
        self.plugin_install_button.setObjectName("PluginInstallButton")
        self.plugin_install_button.setText(self.plugin_install_button_text)
        self.plugin_install_button.setFixedHeight(30)
        self.plugin_install_button.setFixedWidth(60)

        self.main_widget_layout.addWidget(self.plugin_info)
        self.main_widget_layout.addWidget(self.plugin_install_button)

    def setPluginNameText(self, plugin_name):
        self.plugin_name_text = plugin_name
        self.plugin_name.setText(self.plugin_name_text)

    def setPluginIntroductionText(self, plugin_introduction):
        self.plugin_introduction_text = plugin_introduction
        self.plugin_introduction.setText(self.plugin_introduction_text)


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = PluginStoreWidgetItem()
    UI.setPluginNameText("插件")
    UI.setPluginIntroductionText("介绍")
    UI.show()
    sys.exit(app.exec_())
