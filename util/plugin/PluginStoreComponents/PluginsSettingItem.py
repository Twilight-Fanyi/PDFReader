from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QWidget
# from ui.uicomponents.button.SwitchButton import SwitchButton
from ui.uicomponents.button.DropdownButton import DropdownButton


class PluginsSettingItem(QWidget):

    item_changed_signal = pyqtSignal(tuple)   # ⭐类级别的信号对象❤，可通过类本身访问并连接到其他槽函数

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setObjectName("PluginsSettingItem")
        self.setFixedHeight(100)
        self.plugin_install_button_text = "安装"
        self.plugin_name_text = "插件"
        self.plugin_introduction_text = "插件介绍"

        self.item_state = 0  # 0-关闭， 1-左功能栏， 2-右功能栏
        self.id = 'id'


        self.setupUi()
        self.setStyleSheet("#PluginsSettingItem{\n"
                           "    background:rgb(255,255,255);\n"
                           "    font-size:15px;\n"
                           "    font-family:Century Gothic,sans-serif;\n"
                           "    border: 1px solid black;\n"
                           "}\n"
                           "#PluginName{\n"
                           "    font-size:20px;\n"
                           "    font-weight: bold\n"
                           "}\n"
                           "#PluginIntroduction{\n"
                           "color:rgb(132,133,133);\n"
                           "}\n")

    def setupUi(self):
        self.main_widget_layout = QtWidgets.QHBoxLayout()
        self.setLayout(self.main_widget_layout)
        self.plugin_info = QtWidgets.QWidget()
        self.setObjectName("PluginInfo")
        self.plugin_info_layout = QtWidgets.QVBoxLayout()
        self.plugin_info.setLayout(self.plugin_info_layout)

        self.plugin_name = QtWidgets.QLabel()  # ⭐ 插件名称 Label
        self.plugin_name.setObjectName("PluginName")
        self.plugin_name.setText(self.plugin_name_text)

        self.plugin_introduction = QtWidgets.QLabel()  # ⭐ 插件介绍 Label
        self.plugin_introduction.setObjectName("PluginIntroduction")
        self.plugin_introduction.setText(self.plugin_introduction_text)

        self.plugin_info_layout.addWidget(self.plugin_name)
        self.plugin_info_layout.addWidget(self.plugin_introduction)

        # self.plugin_enable_button = SwitchButton()  # ⭐ 安装按钮
        self.plugin_enable_button = DropdownButton()
        # self.plugin_enable_button.setParent(self)
        # self.plugin_enable_button.handleSelectionChange(self.setItemStateSameAsButtonState)
        self.plugin_enable_button.currentIndexChanged.connect(self.setItemStateSameAsButtonState)

        self.plugin_enable_button.setObjectName("PluginEnableButton")
        # self.plugin_enable_button.setText(self.plugin_enable_button_text)
        # self.plugin_enable_button.setFixedHeight(30)
        # self.plugin_enable_button.setFixedWidth(100)
        # self.plugin_introduction.setContentsMargins(50,50,50,50)

        self.main_widget_layout.addWidget(self.plugin_info,1, Qt.AlignLeft )
        self.main_widget_layout.addWidget(self.plugin_enable_button,1, Qt.AlignRight)


    def setPluginNameText(self, plugin_name):
        self.plugin_name_text = plugin_name
        self.plugin_name.setText(self.plugin_name_text)

    def setPluginIntroductionText(self, plugin_introduction):
        self.plugin_introduction_text = plugin_introduction
        self.plugin_introduction.setText(self.plugin_introduction_text)


    def setItemStateSameAsButtonState(self, index):
        self.item_state = index

        # print(self.item_state)
        # self.emit_signal()
        data = (self.id, self.item_state)
        self.item_changed_signal.emit(data)

    # TODO:针对不同方法加入不同的 signal，以及对不同 signal 的判断
    # 此方法即将废除
    def emit_signal(self):
        data = (self.id, self.item_state)
        self.item_changed_signal.emit(data)

    def setID(self, id):
        self.id = id

# 测试 emit_signal 用，emit_signal 已能正常使用
def handle_signal(data):
    # print(data)
    pass




if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = PluginsSettingItem()
    UI.setPluginNameText("插件")
    UI.setPluginIntroductionText("介绍")
    # UI.item_changed_signal.connect(handle_signal)
    # UI.emit_signal()
    UI.show()
    sys.exit(app.exec_())
