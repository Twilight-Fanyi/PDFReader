
import os.path
import sys

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import Qt, QPropertyAnimation, QRect, pyqtProperty, pyqtSignal, QStringListModel
from PyQt5.QtGui import QPainter, QColor, QPen, QBrush, QPixmap
from PyQt5.QtWidgets import QWidget, QApplication, QDialog, QFileDialog, QAction
# from BookCoverWidget import BookCoverWidget
from util.plugin.PluginStoreComponents.PluginStoreBarWidget import PluginStoreBarWidget
from util.plugin.PluginStoreComponents.PluginStoreWidget import PluginStoreWidget
from util.plugin.PluginStoreComponents.PluginsSettingWidget import PluginsSettingWidget
from util.database.ebook_database import EbookDatabase

from util.parsers.pdf_parser import PDF_Parser

from datetime import datetime
from ui.uicomponents.mainfunc.PDFReaderMainWindowComponent.Ui_PDFReaderMainWindow_light import Ui_PDFReaderMainWindow

"""
⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐

⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐
"""


class PluginStore(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setObjectName("BookSelectionMainUI")
        # self.setFixedSize(1100, 650)
        self.windows = []
        self.setupUi()
        self.setStyleSheet("BookSelectionMainUI{background:rgb(255,255,255)}")

    def setupUi(self):
        self.main_widget_layout = QtWidgets.QHBoxLayout(self)
        # self.main_widget_layout.setContentsMargins(0, 0, 0, 0)
        # self.book_cover_widget = BookCoverWidget()
        self.plugin_store_bar_widget = PluginStoreBarWidget()


        self.plugin_store_bar_widget.addFeatureItem("插件设置", self.featureItem_pluginSetting)
        self.plugin_store_bar_widget.addFeatureItem("插件商店", self.featureItem_pluginStore)
        self.plugin_store_widget = PluginsSettingWidget()
        self.main_widget_layout.addWidget(self.plugin_store_bar_widget)
        self.main_widget_layout.addWidget(self.plugin_store_widget)

        self.plugin_store_bar_widget.addSpacer()
        # self.main_widget_layout.addWidget(self.plugin_store_widget)

    def resize_event(self, event):
        self.main_widget_layout.invalidate()  # 使布局无效
        self.main_widget_layout.update()  # 更新布局并重新调整控件大小

    def cancelPluginStoreWidget(self):
        self.plugin_store_widget.deleteLater()
        self.main_widget_layout.update()
    def featureItem_pluginSetting(self):
        self.cancelPluginStoreWidget()
        self.plugin_store_widget = PluginsSettingWidget()
        self.main_widget_layout.addWidget(self.plugin_store_widget)

    def featureItem_pluginStore(self):
        self.cancelPluginStoreWidget()
        self.plugin_store_widget = PluginStoreWidget()
        self.main_widget_layout.addWidget(self.plugin_store_widget)

if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = PluginStore()
    UI.show()
    sys.exit(app.exec_())
