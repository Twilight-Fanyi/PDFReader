from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget

from ui.uicomponents.mainfunc.PDFReaderMainWindowComponent.FeatureBarWidget import FeatureBarWidget


class MDHelper(FeatureBarWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.addFeatureItem("Feature", FeatureBarWidget.openFileAction)
        self.addFeatureWithLabelAndButton("a", "b", FeatureBarWidget.openFileAction)



if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    UI = MDHelper()
    UI.addFeatureItem("Feature", UI.openFileAction)
    UI.addFeatureWithLabelAndButton("a", "b", UI.openFileAction)
    UI.addSpacer()
    UI.show()
    sys.exit(app.exec_())