import collections
import os
import textile

"""TXT_Parser 类用于对 txt 类型文件的解析"""
class TXT_Parser:

    def __init__(self, filename, *args):
        self.filename = filename

    def read_book(self):
        pass

    def generate_metadata(self):
        title = os.path.basename(self.filename)
        author = 'Unknown'
        year = 9999
        isbn = None
        tags = []
        cover = None

        Metadata = collections.namedtuple(
            'Metadata', ['title', 'author', 'year', 'isbn', 'tags', 'cover'])
        return Metadata(title, author, year, isbn, tags, cover)

    def generate_content(self):
        """Generate content of the book."""
        with open(self.filename, 'rt', encoding='UTF-8') as txt:
            text = txt.read()
            content = [textile.textile(text)]

        toc = [(1, 'Text', 1)]

        return toc, content, False



if __name__ == '__main__':
    parse = TXT_Parser('F:\\红楼梦.txt')
    toc,content,bo = parse.generate_content()
    print(content)
