import os
import collections

import fitz
from PyQt5 import QtGui

class PDF_Parser:
    def __init__(self, filename, *args):
        self.filename = filename
        self.book = None

    # 读取 ebook
    def read_book(self):
        self.book = fitz.open(self.filename)

    # 生成 ebook 信息
    def generate_metadata(self):
        title = self.book.metadata['title']
        if not title:
            title = os.path.splitext(os.path.basename(self.filename))[0]

        author = self.book.metadata['author']
        if not author:
            author = 'Unknown'

        creation_date = self.book.metadata['creationDate']
        try:
            year = creation_date.split(':')[1][:4]
        except (ValueError, AttributeError):
            year = 9999

        isbn = None

        tags = self.book.metadata['keywords']
        if not tags:
            tags = []

        cover_page = self.book.load_page(0)
        # Disabling scaling gets the covers much faster
        cover = render_pdf_page(cover_page, True)

        Metadata = collections.namedtuple(
            'Metadata', ['title', 'author', 'year', 'isbn', 'tags', 'cover'])
        return Metadata(title, author, year, isbn, tags, cover)

    # 生成 PDF 内容
    def generate_content(self):
        content = list(range(self.book.page_count))
        toc = self.book.get_toc()
        if not toc:
            toc = [(1, f'Page {i + 1}', i + 1) for i in range(self.book.page_count)]

        # Return toc, content, images_only
        return toc, content, True

    def generate_first_page(self):
        first_page = render_pdf_page(self.book.load_page(0), True)
        return first_page


# 获取 PDF 页数据
def render_pdf_page(page_data, for_cover=False):
    # Draw page contents on to a pixmap
    # and then return that pixmap

    # Render quality is set by the following
    zoom_matrix = fitz.Matrix(4, 4)
    if for_cover:
        zoom_matrix = fitz.Matrix(1, 1)

    pagePixmap = page_data.get_pixmap(
        matrix=zoom_matrix,
        alpha=False)  # Sets background to White
    imageFormat = QtGui.QImage.Format_RGB888  # Set to Format_RGB888 if alpha
    pageQImage = QtGui.QImage(
        pagePixmap.samples,
        pagePixmap.width,
        pagePixmap.height,
        pagePixmap.stride,
        imageFormat)

    # The cover page doesn't require conversion into a Pixmap
    if for_cover:
        return pageQImage

    pixmap = QtGui.QPixmap()
    pixmap.convertFromImage(pageQImage)
    return pixmap

def generatePDFView(self):
    file_path = ''
    self.doc = fitz.open(file_path)
    
    pix = self.doc

if __name__ == "__main__":
    from PyQt5 import QtCore, QtGui, QtWidgets
    from PyQt5.QtWidgets import QFileDialog
    from PIL import Image, ImageQt
    import sys
    app = QtWidgets.QApplication(sys.argv)
    wind = QtWidgets.QWidget()
    label = QtWidgets.QLabel(wind)
    path, info = QtWidgets.QFileDialog().getOpenFileName()
    pdf = PDF_Parser(path)
    pdf.read_book()
    page_data = pdf.book.load_page(1)
    pdf_page = render_pdf_page(page_data, True)
    # page_d = QtGui.QPixmap(pdf_page[0])
    label.setPixmap(pdf_page)

    wind.show()
    sys.exit(app.exec_())


