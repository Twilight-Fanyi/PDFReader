import os
import logging
import collections

import markdown

logger = logging.getLogger(__name__)


class MARKDOWN_Parser:
    def __init__(self, filename, *args):
        self.book = None
        self.filename = filename

    def read_book(self):
        self.book = None

    def generate_metadata(self):
        title = os.path.basename(self.filename)
        author = 'Unknown'
        year = 9999
        isbn = None
        tags = []
        cover = None

        Metadata = collections.namedtuple(
            'Metadata', ['title', 'author', 'year', 'isbn', 'tags', 'cover'])
        return Metadata(title, author, year, isbn, tags, cover)

    def generate_content(self):
        with open(self.filename, 'r', encoding='UTF-8') as book:
            text = book.read()
            content = [markdown.markdown(text)]

        toc = [(1, 'Markdown', 1)]

        # Return toc, content, images_only
        return toc, content, False



if __name__ == "__main__":
    parse = MARKDOWN_Parser('F:\\text.md')
    to, co, bo = parse.generate_content()
    print(co)